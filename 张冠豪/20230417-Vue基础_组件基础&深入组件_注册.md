# 4.17
# 组件基础
## 监听事件
1. 监听 父组件监听子组件的指定的事件
2. 暴露 子组件暴露内部的指定事件，由父组件捕获
### 方法一
子组件可以通过调用内置的 $emit 方法，通过传入事件名称来抛出一个事件

App.vue文件
```JS
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import {ref} from 'vue'


const fontSize=ref(1);

const pp=function(){
  console.log('你好');
  fontSize.value=fontSize.value+0.2;
}

const yy=function(){
  console.log('再见');
  fontSize.value=fontSize.value-0.2
}
</script>

<template>
  <div :style="{fontSize:fontSize+'em'}">
    小红
    <HelloWorld @pp4="pp" @yy3="yy"/>
  </div>

</template>
```
HelloWorld.vue文件
```JS
<script setup>

</script>

<template>
<div>
  <input type="button" value="点我变大" @click="$emit('pp4')">
  <input type="button" value="点我变小" @click="$emit('yy3')">
</div>
</template>
```
### 方法2
我们可以通过 defineEmits 宏来声明需要抛出的事件：

App.vue文件
```JS
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import {ref} from 'vue'


const fontSize=ref(1);

const pp=function(){
  console.log('你好');
  fontSize.value=fontSize.value+0.2;
}

const yy=function(){
  console.log('再见');
  fontSize.value=fontSize.value-0.2
}
</script>

<template>
  <div :style="{fontSize:fontSize+'em'}">
    小红
    <HelloWorld @pp4="pp" @yy3="yy"/>
  </div>

</template>
```
HelloWorld文件
```JS
<script setup>
const emit=defineEmits(['yy3','pp4'])


const yy=function(){
  emit('yy3')
}

const pp=function(){
  emit('pp4')
}
</script>

<template>
<div>
  <input type="button" value="点我变大" @click="yy">
  <input type="button" value="点我变小" @click="pp">
</div>
</template>

```
## 通过插槽来分配内容
通过 Vue 的自定义 <slot> 元素来实现 我们使用 <slot> 作为一个占位符，父组件传递进来的内容就会渲染在这里
App.vue文件
```JS
<HelloWorld>小红</HelloWorld>
```
HelloWorld文件
```JS
 <slot />
```
## 动态组件
```JS

<script setup>
// import HelloWorld from './components/HelloWorld.vue'
import S1 from './components/Stu1.vue'
import S2 from './components/Stu2.vue'
import S3 from './components/Stu3.vue'
import {ref,reactive, shallowRef, markRaw} from 'vue'

const currentTab=shallowRef(null);
const obj=markRaw({
  S1:S1,
  S2:S2,
  S3:S3

})


const btnConvert=function(i){
  // let rnd=Math.floor(Math.random()*3)+1
  currentTab.value=obj['S'+i]
}

</script>

<template>
  <div>
    <input type="button" value="stu1" @click="btnConvert(1)">
    <input type="button" value="stu2" @click="btnConvert(2)">
    <input type="button" value="stu3" @click="btnConvert(3)">
    <br>
    <component :is="currentTab"></component>
  </div>
</template>

```
当使用 <component :is="...">来在多个组件间作切换时，被切换掉的组件会被卸载
## DOM模板解析注意事项
- 大小写区分
- 闭合标签
- 元素位置限制
一些HTML元素对于放在其中的元素类型有限制。例如<ul>，<ol>，<table> 和 <select>，相应的，某些元素仅在放置于特定元素中时才会显示，例如 <li>，<tr> 和 <option>
# 深入组件
## 注册
一个 Vue 组件在使用前需要先被“注册”，这样 Vue 才能在渲染模板时找到其对应的实现。组件注册有两种方式：`全局注册`和`局部注册`
### 全局注册
使用app.component()方法，让组件在当前Vue应用中全局可用
### 局部注册
全局注册虽然很方便，但有以下几个问题：

全局注册，但并没有被使用的组件无法在生产打包时被自动移除 (也叫“tree-shaking”)。如果你全局注册了一个组件，即使它并没有被实际使用，它仍然会出现在打包后的 JS 文件中

全局注册在大型项目中使项目的依赖关系变得不那么明确。在父组件中使用子组件时，不太容易定位子组件的实现。和使用过多的全局变量一样，这可能会影响应用长期的可维护性

相比之下，局部注册的组件需要在使用它的父组件中显式导入，并且只能在该父组件中使用。它的优点是使组件之间的依赖关系更加明确，并且对 tree-shaking 更加友好

在使用 <script setup> 的单文件组件中，导入的组件可以直接在模板中使用，无需注册： 
```JS
<script setup>
// import HelloWorld from './components/HelloWorld.vue'
import S1 from './components/Stu1.vue'
import S2 from './components/Stu2.vue'
import S3 from './components/Stu3.vue'




</script>

<template>
  <S1 />
</template> 
```
### 组件名格式
在整个指引中，我们都使用 PascalCase 作为组件名的注册格式，这是因为：

1. PascalCase 是合法的 JavaScript 标识符。这使得在 JavaScript 中导入和注册组件都很容易，同时 IDE 也能提供较好的自动补全

2. <PascalCase /> 在模板中更明显地表明了这是一个 Vue 组件，而不是原生 HTML 元素。同时也能够将 Vue 组件和自定义元素 (web components) 区分开来

在单文件组件和内联字符串模板中，我们都推荐这样做。但是，PascalCase 的标签名在 DOM 模板中是不可用的，详情参见 DOM 模板解析注意事项

为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。这意味着一个以 MyComponent 为名注册的组件，在模板中可以通过 <MyComponent> 或 <my-component> 引用。这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板