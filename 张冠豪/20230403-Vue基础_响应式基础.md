# 4.3笔记

## 接3.31笔记模板语法还有一些没讲完
### 指令 Directives
- v-if - 根据表达式的值条件性地渲染元素。如果表达式为真，则元素会被渲染；否则，元素将被删除。可以使用 v-else 和 v-else-if 来添加其他条件分支

- v-for - 基于源数据多次渲染元素或块。v-for 通过遍历数组或对象中的项来生成内容。可以使用 v-for 的特殊语法来访问数组或对象的当前项、索引和键

- v-on - 绑定事件监听器，触发回调函数。可以使用 v-on 绑定常见的 DOM 事件（如 click、input、submit 等）以及自定义事件。可以使用 event 对象访问原生事件，也可以通过 $event 访问原生事件的属性和方法

- v-bind - 动态地绑定一个或多个特性，或一个组件 prop 到表达式。可以使用 : 缩写语法，例如 :href="url"。可以使用动态绑定表达式来计算特性或 prop 的值

- v-model - 在表单输入控件上创建双向数据绑定。v-model 绑定到一个数据属性，并在用户输入时自动更新该属性。可以使用 v-model 来绑定输入控件的值、选中状态、多选列表等

- v-text - 更新元素的 textContent。和 {{ }} 的插值语法类似，可以使用 v-text 来直接更新元素的文本内容，而不是使用 textContent 属性

- v-html - 更新元素的 innerHTML。可以使用 v-html 来直接将字符串作为 HTML 插入到元素中。但是要注意潜在的安全风险，因为插入的内容可能包含恶意脚本或标签

## 声明响应式状态
选用选项式 API 时，会用`data`选项来声明组件的响应式状态。此选项的值应为返回一个对象的函数

虽然也可以不在`data`上定义，直接向组件实例添加新属性，但这个属性将无法触发响应式更新

## 声明方法
要为组件添加方法，我们需要用到` methods `选项。它应该是一个包含所有方法的对象：
```JS
<script>
export default{
  mounted(){
    console.log(1111+'hhh');
    this.add();
    this.bbc();
    this.ok() //undefind
  },
  methods:{
    add(){
      console.log('111');
      console.log(this);
    },
    bbc:function(){
      console.log('222');
      console.log(this);
    },
    ok:()=>{
      console.log('333');
      console.log(this);
    }
  }
}
</script>
```
Vue 自动为`methods`中的方法绑定了永远指向组件实例的`this`。这确保了方法在作为事件监听器或回调函数时始终保持正确的`this`。你不应该在定义 `methods`时使用箭头函数，因为箭头函数没有自己的`this`上下文

## 选项式
```JS
<script>
export default{
  data(){
    return{
      msg:'好手机',
      list:[
        {
          id:1,
          productName:'苹果200',
          price:'16999',
        },
        {
          id:2,
          productName:'菠萝2000',
          price:'17999',
        },
        {
          id:3,
          productName:'樱桃3000',
          price:'18999',
        },
        {
          id:4,
          productName:'香蕉900',
          price:'19999',
        },
      ]
    }
  },
  methods:{
    rnd(){
      let r=Math.floor(Math.random()*4) ;
      console.log(r);
      let str='好手机贵点咋了'
      let rndName=str.substring(Math.floor(Math.random()*str.length),1)  
      this.list[r].productName=rndName
    }
  }
}
</script>

<template>
  <table>
    <tr>
      <th>Id</th>
      <th>产品名称</th>
      <th>价格</th>
      <th>操作</th>
    </tr>
    <tr v-for="item in list">
      <td>
        {{ item.id }}
      </td>
      <td>
        {{ item.productName }}
      </td>
      <td>
        {{ item.price }}
      </td>
      <td>
        <input type="button" value="编辑">
        <input type="button" value="删除">
      </td>
    </tr>
  </table>
  <input type="text" v-model="msg">
  <input type="button" value="改变" @click="rnd">
  {{ msg }}
</template>
```
## 组合式
```JS
<script setup>
import { reactive } from 'vue';
let list=reactive([
  {
          id:1,
          productName:'苹果200',
          price:'16999',
        },
        {
          id:2,
          productName:'菠萝2000',
          price:'17999',
        },
        {
          id:3,
          productName:'樱桃3000',
          price:'18999',
        },
        {
          id:4,
          productName:'香蕉900',
          price:'19999',
        },
      ])


    function rnd(){
      let r=Math.floor(Math.random()*4) ;
      console.log(r);
      let str='好手机贵点咋了'
      let rndName=str.substring(Math.floor(Math.random()*str.length),1)  
      this.list[r].productName=rndName
    }
  

</script>

<template>
  <table>
    <tr>
      <th>Id</th>
      <th>产品名称</th>
      <th>价格</th>
      <th>操作</th>
    </tr>
    <tr v-for="item in list">
      <td>
        {{ item.id }}
      </td>
      <td>
        {{ item.productName }}
      </td>
      <td>
        {{ item.price }}
      </td>
      <td>
        <input type="button" value="编辑">
        <input type="button" value="删除">
      </td>
    </tr>
  </table>
  <input type="text" v-model="msg">
  <input type="button" value="改变" @click="rnd">
  {{ msg }}
</template>

```
## DOM更新时机
当你更改响应式状态后，DOM 会自动更新。然而，你得注意 DOM 的更新并不是同步的。相反，Vue 将缓冲它们直到更新周期的 “下个时机” 以确保无论你进行了多少次状态更改，每个组件都只更新一次