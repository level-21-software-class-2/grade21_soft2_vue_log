# 4.27笔记
# 路由匹配语法
```JS
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import {createRouter,createWebHistory} from 'vue-router'

const routes=[
    {
        path:'/',
        component:()=>import ('./components/Blog.vue')
    },
    {
        path:'/:name',
        component:()=>import('./components/BlogName.vue')
    },
    {
        path:'/:id',
        component:()=>import('./components/BlogId.vue')
    }
]

const router=createRouter({
    history:createWebHistory(),
    routes
})



createApp(App).use(router).mount('#app')
```
routes 数组的顺序重要! 具有顺序关系，匹配了第一个就不会继续往下匹配，所以/name匹配第二条，/3也是匹配第二条
## 在参数中自定义正则
(\\d+)可以匹配数字
```JS
const routes = [
  // /:orderId -> 仅匹配数字
  { path: '/:orderId(\\d+)' },
  // /:productName -> 匹配其他任何内容
  { path: '/:productName' },
]
```
routes 数组的顺序并不重要!
## 可重复的参数
如果你需要匹配具有多个部分的路由，如 /first/second/third，你应该用 *（0 个或多个）和 +（1 个或多个）将参数标记为可重复
```JS
const routes = [
  // /:chapters ->  匹配 /one, /one/two, /one/two/three, 等
  { path: '/:chapters+' },
  // /:chapters -> 匹配 /, /one, /one/two, /one/two/three, 等
  { path: '/:chapters*' },
]
```
这些也可以通过在右括号后添加它们与自定义正则结合使用

## Sensitive 与 strict 路由配置
- 默认情况下，所有路由是不区分大小写的，并且能匹配带有或不带有尾部斜线的路由
- 可以通过 strict 和 sensitive 选项来修改，它们可以既可以应用在整个全局路由上，又可以应用于当前路由上
## 可选参数
- - 你也可以通过使用 ? 修饰符(0 个或 1 个)将一个参数标记为可选
请注意，* 在技术上也标志着一个参数是可选的，但 ? 参数不能重复
# 嵌套路由
## main.js文件
要将组件渲染到这个嵌套的 router-view 中，我们需要在路由中配置 children
注意，以 / 开头的嵌套路径将被视为根路径。这允许你利用组件嵌套，而不必使用嵌套的 URL
```JS
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'



const routes = [
 
    {
        path: '/Class',
        component: () => import('./components/Class.vue'),
        children:[
            {
                path:'SXiaoHong',
                component:() =>import('./components/SXiaoHong.vue')
            },
            {
                path:'TXiaoHong',
                component:() =>import('./components/TXiaoHong.vue')
            }
        ]
    }

]



const router = createRouter({
    history: createWebHistory(),
    routes
})

createApp(App).use(router).mount('#app')

```
## Class.vue文件
Class.vue 它渲染顶层路由匹配的组件。同样地，一个被渲染的组件也可以包含自己嵌套的 <router-view>
```JS
<template>
    <div id="class">
        21级2班
        <router-view></router-view>
    </div>
</template>

<style>
    #class{
        height: 200px;
        width: 400px;
        background-color: grey;
    }

</style>
```
## SXiaoHpng.vue文件
```JS
<template>
    <div id="sh">
        小红同学
    </div>
</template>

<style>
    #sh{
        height:100px;
        width: 400px;
        background-color: red;
    }
</style>
```
## TXiaoHpng.vue文件
```JS
<template>
    <div id="th">
        小红老师
    </div>
</template>

<style>
    #th{
        height: 100px;
        width: 400px;
        background-color: rebeccapurple;
    }
</style>
```
## App.vue文件
```JS
<template>
  <router-view></router-view>
</template>

```
## 嵌套命名路由
在处理命名路由时，你通常会给子路由命名
```JS
const routes = [
  {
    path: '/user/:id',
    component: User,
    // 请注意，只有子路由具有名称
    children: [{ path: '', name: 'user', component: UserHome }],
  },
]
```
在一些场景中，你可能希望导航到命名路由而不导航到嵌套路由。例如，你想导航 /user/:id 而不显示嵌套路由。那样的话，你还可以命名父路由，但请注意重新加载页面将始终显示嵌套的子路由，因为它被视为指向路径/users/:id 的导航，而不是命名路由