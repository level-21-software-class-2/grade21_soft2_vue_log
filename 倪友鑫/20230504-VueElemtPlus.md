# ElementPlus
ElementPlus 是一套为开发者、设计师和产品经理准备的基于 Vue 3.0 的组件库，提供了配套设计资源，帮助你的网站快速成型。
# 安装
npm install elememt-plus

# 引入
```js
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```