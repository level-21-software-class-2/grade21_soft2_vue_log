# PROPS
``` vue
<script setup>
const props =defineProps({id:String,Name:String})
</script>

<template>
 <div>
  <h1>
    {{ id }}
  </h1>
  {{ Name }}
 </div>
</template>

<script setup>
import HelloWorld from './components/HelloWorld.vue';
import { reactive } from 'vue';

const obj = reactive({
    id:'1',
    Name:'123'
})


</script>

<template>
  <HelloWorld v-bind="obj"></HelloWorld>
</template>
```
# Props名字格式
``` vue
//定义了一个很长的名字(小驼峰)
export default {
  props: {
    greetingMessage: String
  }
}
//使用的时候就是必须和定义的一模一样
<span>{{ greetingMessage }}</span>
//推荐这种命名格式，也可以不加-，直接和原来的一样
<MyComponent greeting-message="hello" />
```