# 页面渲染
## router
router:Vue Router 路径对象

在`<el-menu>`设置`:router="true"`

## main
写路径
```js
import Right1 from './views/ViewRight1.vue'
import Right2 from './views/ViewRight2.vue'
import Right3 from './views/ViewRight3.vue'
import Home from './components/Home.vue'

const routes = [
    {
        path: '/',
        component: Home,
        children:[
            {
                path: '/right1',
                component: Right1
            },
            {
                path: '/right2',
                component: Right2,
            },
            {
                path: '/right3',
                component: Right3,
            }
        ]
    },
    
]
```

## routerview
路径出口

## 路径匹配
```vue
<script setup>
import { ref, reactive } from 'vue';
import ViewLeft from './ViewLeft.vue';

const isCollapse = ref(false)
const menuList = reactive([
  {
    title: '系统管理',
    meta: { icon: 'setting' },
    children: [
      {
        title: '用户管理',
        url: '/',
        url: '/right1',
        meta: { icon: 'User' }
      },
      {
        title: '角色管理',
        url: '/right2',
        meta: { icon: 'Grid' }
      },
      {
        title: '权限管理',
        url: '/right3',
        meta: { icon: 'Promotion' }
      },
      {
        title: '部门管理',
        meta: { icon: 'HelpFilled' },
        children: [
          {
            title: '销售部',
            url: '/right1',
            meta: { icon: 'user' }
          },
          {
            title: '研发部',
            url: '/right2',
            meta: { icon: 'user' }
          },
          {
            title: '财务部',
            url: '/right3',
            meta: { icon: 'user' },
            children: [
              {
                title: '出纳',
                url: '/right1',
              },
              {
                title: '会计',
                url: '/right2',

              },
              {
                title: '财务总监',
                url: '/right3',
              },
            ]
          },
        ]
      },
    ]
  }, {
    title: '学生管理',
    url: '/right3',
    meta: { icon: 'setting' },
  }
])

const iconClickHandler = function () {
  console.log('888');
  isCollapse.value = !isCollapse.value
}
</script>

<template>
  <div>
    <div class="common-layout">
      <el-container>
        <el-aside :width="isCollapse ? '64px' : '200px'">
          <ViewLeft :is-collapse="isCollapse" :menus="menuList"></ViewLeft>
        </el-aside>
        <el-container>
          <el-header>
            <el-icon :class="{ rotateClass: isCollapse }" :size="30" @click="iconClickHandler">
              <fold></fold>
            </el-icon>
          </el-header>
          <!-- <el-main> -->
              <RouterView></RouterView>
          <!-- </el-main> -->
          <el-footer>

            <el-row>
              <el-col :span="14">
                <div class="mar1">
                  Copyright ©2023 让我康康商标专利事务所 All Rights Reserved.
                </div>
              </el-col>
              <el-col :span="10">
                <div class="mar2">
                  <a href="/" id="aaa">
                    <el-image src="../public/icon.png"></el-image>
                    <!-- <img src="../public/icon.png" alt=""> -->
                    粤ICP备20057364号
                  </a>
                </div>
              </el-col>
            </el-row>
            <el-row>
              <el-col :span="24">
                <div class="top">
                  声明：转载内容版权归作者及来源网站所有，本站原创内容转载请注明来源。
                </div>
              </el-col>
            </el-row>
          </el-footer>
        </el-container>
      </el-container>
    </div>

  </div>
</template>

<style scoped>
.el-image{
  vertical-align: top;
}
.el-row{
  margin-top: 5px;
}
#aaa {
  margin-left: 5px;
}

.el-aside {
  background-color: #4de0eb;
  height: calc(100vh);
  /* flex: 2 2 auto; */
}

.el-header {
  background-color: rgb(45, 98, 212);
  display: flex; 
  align-items: center;
}

.el-main {
  background-color: rgb(16, 120, 218);
}

.el-footer {
  background-color: rgb(11, 219, 140);
}

.rotateClass {
  transform: rotate(-90deg);
}
.mar1{
  text-align:right
}
.mar2{
  /* text-align:center */
  margin-left: 50px;
}
.top{
  /* text-align:center */
  margin-left: 250px;
}
</style>
```