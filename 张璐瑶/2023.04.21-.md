## 使用一个对象绑定多个 prop​
如果你想要将一个对象的所有属性都当作 props 传入，你可以使用没有参数的 v-bind，即只使用 v-bind 而非 :prop-name。例如，这里有一个 post 对象：

```js
export default {
  data() {
    return {
      post: {
        id: 1,
        title: 'My Journey with Vue'
      }
    }
  }
}
```
以及下面的模板：

```template
<BlogPost v-bind="post" />
```
而这实际上等价于：

```template
<BlogPost :id="post.id" :title="post.title" />
```