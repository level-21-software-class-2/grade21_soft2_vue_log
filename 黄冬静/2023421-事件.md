# 触发与监听事件
在组件的模块表达式中，可以直接使用$emit方法触发自定义事件
```js
<!-- MyComponent -->
<button @click="$emit('someEvent')">click me</button>
```
$emit()方法在组件实例相上也同样以this.$emit()的形式可用：

```js
export default {
    methods:{
        submit(){
            this.$emit('someEvent')
        }
    }
}
```
父组件可以通过 v-on (缩写为 @) 来监听事件：
```js
<MyComponent @some-event="callback" />
```
同样，组件的事件监听器也支持 .once 修饰符：
```js
<MyComponent @some-event.once="callback" />
```

# 声明触发事件
```js
export default {
  emits: ['inFocus', 'submit']
}
```

# 事件效验
要为事件添加校验，那么事件可以被赋值为一个函数，接受的参数就是抛出事件时传入 this.$emit 的内容，返回一个布尔值来表明事件是否合法。
```js
export default {
  emits: {
    // 没有校验
    click: null,

    // 校验 submit 事件
    submit: ({ email, password }) => {
      if (email && password) {
        return true
      } else {
        console.warn('Invalid submit event payload!')
        return false
      }
    }
  },
  methods: {
    submitForm(email, password) {
      this.$emit('submit', { email, password })
    }
  }
}
```