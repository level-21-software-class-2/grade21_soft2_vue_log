# 事件处理
```js
<script>
export default{
    data(){
      return {
        count:0
      }
    },
    methods:{
      name(){
        console.log('这是name');
      },
      abc(){
        console.log('这是abc');
      },
    
    say(message) {
    alert(message)
  }

    }
    
  
}
</script>

<template>
    <div @click="abc">
      <button @click="count++">{{ count }}</button><br>
      <button @click="name">点我一下</button>
      <button @click="say('hello静静')">Say hello</button>
      <button @click="say('bye小静')">Say bye</button>  

    </div>
</template>

```

# 事件修饰符
.stop  @click.stop单击事件将停止传递
.prevent @submit.prevent="onSubmit"提交事件将不再重新加载页面 
.self @click.self<!-- 仅当 event.target 是元素本身时才会触发事件处理器 -->
<!-- 例如：事件处理器不来自子元素 -->
.capture
.once
.passive

修饰语可以使用链式书写  @click.stop.prevent=
# 按键修饰符

在监听键盘事件时，我们经常需要检查特定的按键。Vue 允许在 v-on 或 @ 监听按键事件时添加按键修饰符。

template
<!-- 仅在 `key` 为 `Enter` 时调用 `submit` -->
<input @keyup.enter="submit" />
你可以直接使用 KeyboardEvent.key 暴露的按键名称作为修饰符，但需要转为 kebab-case 形式。

template
<input @keyup.page-down="onPageDown" />
在上面的例子中，仅会在 $event.key 为 'PageDown' 时调用事件处理。

# 按键别名​
Vue 为一些常用的按键提供了别名：

.enter
.tab
.delete (捕获“Delete”和“Backspace”两个按键)
.esc
.space
.up
.down
.left
.right
# 系统按键修饰符​
你可以使用以下系统按键修饰符来触发鼠标或键盘事件监听器，只有当按键被按下时才会触发。

.ctrl
.alt
.shift
.meta
注意

在 Mac 键盘上，meta 是 Command 键 (⌘)。在 Windows 键盘上，meta 键是 Windows 键 (⊞)。在 Sun 微机系统键盘上，meta 是钻石键 (◆)。在某些键盘上，特别是 MIT 和 Lisp 机器的键盘及其后代版本的键盘，如 Knight 键盘，space-cadet 键盘，meta 都被标记为“META”。在 Symbolics 键盘上，meta 也被标识为“META”或“Meta”。
```js
举例来说：

template
<!-- Alt + Enter -->
<input @keyup.alt.enter="clear" />

<!-- Ctrl + 点击 -->
<div @click.ctrl="doSomething">Do something</div>
```
# 鼠标按键修饰符
.left
.right
.middle