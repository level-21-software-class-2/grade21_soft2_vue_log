## App.vue
```js
<script setup>


</script>

<template>
 <Router-view></Router-view>
</template>
```
## add.vue
```js
<template>
    这里是add
    <p>
        <router-link to="/ddd">返回ddd</router-link>
    </p>
</template>
```
## ddd.vue
```js
<template>
   这里是ddd
    <p>
        <router-link to="/add"> 返回add</router-link>
    </p>
</template>
```
## main.js
```js
import { createApp } from 'vue'
import { createWebHistory, createRouter } from 'vue-router'
import './style.css'
import App from './App.vue'
import add from './components/add.vue'
import ddd from './components/ddd.vue'
// createApp(App).mount('#app')

// 1. 定义路由组件.
// 也可以从其他文件导入


// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
    { path: '/add', component: add },
    { path: '/ddd', component: ddd },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: createWebHistory(),
    routes, // `routes: routes` 的缩写
})

// 5. 创建并挂载根实例
const app = createApp(App)
//确保 _use_ 路由实例使
//整个应用支持路由。
app.use(router)

app.mount('#app')

// 现在，应用已经启动了！
```
