## main.js
```js
import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import Navbar from './components/Navbar.vue'
import Content from './components/Content.vue'

const routes = [
    {
        path: '/',
        components: {
            default: Navbar,
            cc: Content
        }
    }


]

const router = createRouter({
    history: createWebHistory(),
    routes
})

const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(ElementPlus).use(router).mount('#app')
## App.vue
```js
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="300px">
          <Navbar :menus="menuList">
            <el-menu :default-openends="['1','2']">

            </el-menu>
          </Navbar>
      </el-aside>
      <el-container>
        <el-header>Header</el-header>
        <el-main>
          <div class="inline-flex" :style="obj">

          </div>
        </el-main>
        <el-footer>Footer</el-footer>
      </el-container>
    </el-container>
  </div>
</template>
<script setup>
import { reactive } from 'vue';
import Navbar from './components/Navbar.vue';

const menuList = reactive([
  {
    title: '系统管理',
    meta: { icon: 'setting' },
    children: [
      {
        title: '用户管理',
        meta: { icon: 'User' }
      },
      {
        title: '角色管理',
        meta: { icon: 'Grid' }
      },
      {
        title: '权限管理',
        meta: { icon: 'Promotion' }
      },
      {
        title: '部门管理',
        meta: { icon: 'HelpFilled' },
        children: [
          {
            title: '销售部',
            meta: { icon: 'user' }
          },
          {
            title: '研发部',
            meta: { icon: 'user' }
          },
          {
            title: '财务部',
            meta: { icon: 'user' },
            children: [
              {
                title: '出纳'
              },
              {
                title: '会计',
                children: [
                  {
                    title: '初级会计师'
                  },
                  {
                    title: '中级会计师'
                  },
                  {
                    title: '高级会计师'
                  },
                  {
                    title: '注册会计师'
                  },
                ]
              },
              {
                title: '财务总监'
              },
            ]
          },
        ]
      },
    ]
  }, {
    title: '学生管理',
    meta: { icon: 'setting' }
  }
])
const obj = reactive({ boxShadow: 'var("--el-box-shadow-dark")', height: '30px', width: '30px', backgroundColor: 'black' })
</script>
<style scoped>
.el-aside {
  background-color: #636466;
  height: calc(100vh);
  /* flex: 2 2 auto; */
}

.el-header {
  background-color: wheat;
}

.el-main {
  /* background-color: blue; */
}

.el-footer {
  background-color: #A3A6AD;
}
</style>
```
## Navbar.vue
```js
<template>
  <div>
      <el-menu>
          <NavbarItem :itemMenus="menus"></NavbarItem>
      </el-menu>
  </div>
</template>
<script setup>
const props = defineProps(['menus'])
import NavbarItem from './NavbarItem.vue'
</script>
NavbarItem
<template>
    <div>
        <!-- 遍历传入的菜单数组 -->
        <template v-for="menu in itemMenus">
            <!-- 当前菜单对象如果有下级菜单，即有children属性，并且children的长度大于0，则需要使用组件中的子菜单组件el-sub-menu -->
            <el-sub-menu v-if="menu.children && menu.children.length > 0">
                <template #title>
                    <el-icon>
                        <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                        <setting v-else></setting>
                    </el-icon>
                    {{ menu.title }}
                </template>
                <!-- 重复自己 -->
                <NavbarItem :item-menus="menu.children"></NavbarItem>
            </el-sub-menu>
            <el-menu-item v-else>
                <el-icon>
                    <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                    <setting v-else></setting>
                </el-icon>
                {{ menu.title }}    
            </el-menu-item>
        </template>
    </div>
</template>
<script setup>
const props = defineProps(['itemMenus'])

</script>
Content.vue
<template>
    <div>
        内容区
    </div>
</template>
<style scoped>
div {
    float: left;
    height: 200px;
    width: 600px;
    background-color: blue;
}
</style>
```