# 模板引用
ref 是一个特殊的 attribute，和 v-for 章节中提到的 key 类似。它允许我们在一个特定的 DOM 元素或子组件实例被挂载后，获得对它的直接引用。这可能很有用，比如说在组件挂载时将焦点设置到一个 input 元素上，或在一个元素上初始化一个第三方库。

## 选项式
```
<script>
export default {
  mounted(){
    this.$refs.input.focus()
  }
}

</script>

<template>
  <input type="text" ref="input">
</template>



```

## 组合式
```
<script setup>
// 声明一个 ref 来存放该元素的引用
// 必须和模板里的 ref 同名
import {ref,onMounted} from 'vue'
const pp=ref(null)

onMounted(()=>{
  pp.value.focus()
})
</script>

<template>
  <input type="text" ref="pp">
</template>



```
注意，你只可以在组件挂载后才能访问模板引用。如果你想在模板中的表达式上访问 input，在初次渲染时会是 null。这是因为在初次渲染前这个元素还不存在呢！

## v-for 中的模板引用​
需要 v3.2.25 及以上版本

当在 v-for 中使用模板引用时，相应的引用中包含的值是一个数组


## 函数模板引用​
除了使用字符串值作名字，ref attribute 还可以绑定为一个函数，会在每次组件更新时都被调用。该函数会收到元素引用作为其第一个参数

## 组件上的 ref

如果一个子组件使用的是选项式 API ，被引用的组件实例和该子组件的 this 完全一致，这意味着父组件对子组件的每一个属性和方法都有完全的访问权。这使得在父组件和子组件之间创建紧密耦合的实现细节变得很容易，当然也因此，应该只在绝对需要时才使用组件引用。大多数情况下，你应该首先使用标准的 props 和 emit 接口来实现父子组件交互。


# 组件基础

## 定义一个组件​
当使用构建步骤时，我们一般会将 Vue 组件定义在一个单独的 .vue 文件中，这被叫做单文件组件 (简称 SFC)
模板是一个内联的 JavaScript 字符串，Vue 将会在运行时编译它。你也可以使用 ID 选择器来指向一个元素 (通常是原生的 <template> 元素)，Vue 将会使用其内容作为模板来源。

## 使用组件

要使用一个子组件，我们需要在父组件中导入它。


## 传递 props

当一个值被传递给 prop 时，它将成为该组件实例上的一个属性。该属性的值可以像其他组件属性一样，在模板和组件的 this 上下文中访问。

一个组件可以有任意多的 props，默认情况下，所有 prop 都接受任意类型的值。