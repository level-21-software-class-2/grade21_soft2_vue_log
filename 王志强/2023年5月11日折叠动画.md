# 折叠动画
使用collapse-transition来关闭默认折叠动画  
默认值为true，修改为false  
修改侧边栏el-aside能自定义折叠动画
```html
<style>
.el-aside {
  transition: width 0.5s;
}
</style>
```