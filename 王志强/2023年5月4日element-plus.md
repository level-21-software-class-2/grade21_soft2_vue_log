# element-plus
## 使用包管理器安装
npm install element-plus --save
## 引入
main.js
```js
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app = createApp(App)

app.use(ElementPlus)

app.mount('#app')
```
## 自适应高度
App.vue
```vue
<template>
  <div id="aa">
    <el-container>
      <el-aside>侧</el-aside>
      <el-container>
        <el-header>头</el-header>
        <el-main>主</el-main>
        <el-footer>尾</el-footer>
      </el-container>
    </el-container>
  </div>
</template>

<style>
  .el-header{
    background-color: aquamarine;
  }
  .el-aside{
    background-color: bisque;
    height: 100vh;
  }
  .el-main{
    background-color: aqua;
  }
  .el-footer{
    background-color: darkgray;
  }
</style>
```