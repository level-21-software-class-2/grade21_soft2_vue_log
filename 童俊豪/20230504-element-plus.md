# Element Plus
## 前端常见的UI组件库：
```
elemet-plus(Vue3.0)
ant-design (多在react实现)
iview(view design)
```

## 安装
```
yarn add element-plus
全局引入：main.js
import ElementPlus from 'element-plus',
import 'element-plus/dist/index.css'
```

## 引入
```
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import {createRouter,createWebHistory} from 'vue-router'


const routes=[
    {
        path:'/',
        component:()=>import('./components/HelloWorld.vue')
    }
]

const router=createRouter({
    history:createWebHistory(),
    routes
})

createApp(App).use(ElementPlus).use(router).mount('#app')
```