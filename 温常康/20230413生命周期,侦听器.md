# 生命周期
## 图示
下面是实例生命周期的图表。你现在并不需要完全理解图中的所有内容，但以后它将是一个有用的参考。
![](./img/shengming.png)
# 侦听器
## 基本示例
```vue
<script >
export default {
  data() {
    return {
      wawa:'hello'
    }
  },
  watch:{
    wawa(newVal,oldVal){
      console.log(newVal);
      if (newVal.endsWith('!')) {
        console.log('safa');
      }
    }
  }
}
</script>
  
<template>
 <div>
  {{ wawa }}
  <input type="text" v-model="wawa">
 </div>
</template>
```
## 组合式侦听器
```vue
<script setup>
import { ref, watch } from 'vue'
import axios from 'axios'
const question = ref('');
const answer = ref('');
const imgUrl = ref('');
watch(question, (newVal, oldVal) => {
  if (newVal.endsWith('?')) {
    console.log('思考');
    axios.get(`http://localhost:8090/users`).then(res => {
      let data = res.data.data;
      console.log(data);
      answer.value = data.answer;
      imgUrl.value = data.imgUrl;
    })
  }
})
</script>
  
<template>
  <div>
    <input type="text" v-model="question">
    {{ answer }}
    <img :src="imgUrl" alt="" style="width: 200pm;">
  </div>
</template>
```
`http://localhost:8090/users`地址内容
![](./img/20230413.png)
# axios
yarn add axios