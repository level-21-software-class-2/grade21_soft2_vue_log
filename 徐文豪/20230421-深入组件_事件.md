# 4.21笔记
# 组件事件
## 触发和监听事件
在组件的模板表达式中，可以直接使用`$emit`方法触发自定义事件
```JS
<!-- MyComponent -->
<button @click="$emit('someEvent')">click me</button>

//$emit() 方法在组件实例上也同样以 this.$emit() 的形式可用：

export default {
  methods: {
    submit() {
      this.$emit('someEvent')
    }
  }
}

//父组件可以通过 v-on (缩写为 @) 来监听事件：

<MyComponent @some-event="callback" />

//同样，组件的事件监听器也支持 .once 修饰符：

<MyComponent @some-event.once="callback" />
```
像组件与 prop 一样，事件的名字也提供了自动的格式转换。注意这里我们触发了一个以 camelCase 形式命名的事件，但在父组件中可以使用 kebab-case 形式来监听。与 prop 大小写格式一样，在模板中我们也推荐使用 kebab-case 形式来编写监听器

## 事件参数
有时候我们会需要在触发事件时附带一个特定的值。举例来说，我们想要 <BlogPost> 组件来管理文本会缩放得多大。在这个场景下，我们可以给 $emit 提供一个额外的参数：
```JS
<button @click="$emit('increaseBy', 1)">
  Increase by 1
</button>

//然后我们在父组件中监听事件，我们可以先简单写一个内联的箭头函数作为监听器，此函数会接收到事件附带的参数：
<MyButton @increase-by="(n) => count += n" />

//或者，也可以用一个组件方法来作为事件处理函数：
<MyButton @increase-by="increaseCount" />

//该方法也会接收到事件所传递的参数：
methods: {
  increaseCount(n) {
    this.count += n
  }
}
```
## 声明触发的事件
组件可以显式地通过 emits 选项来声明它要触发的事件：
```JS
export default {
  emits: ['inFocus', 'submit']
}
```
这个 emits 选项还支持对象语法，它允许我们对触发事件的参数进行验证：
```JS
export default {
  emits: {
    submit(payload) {
      // 通过返回值为 `true` 还是为 `false` 来判断
      // 验证是否通过
    }
  }
}
```
TypeScript 用户请参考：如何为组件所抛出的事件标注类型

尽管事件声明是可选的，我们还是推荐你完整地声明所有要触发的事件，以此在代码中作为文档记录组件的用法。同时，事件声明能让 Vue 更好地将事件和透传 attribute 作出区分，从而避免一些由第三方代码触发的自定义 DOM 事件所导致的边界情况
## 事件校验
```JS
export default {
  emits: {
    // 没有校验
    click: null,

    // 校验 submit 事件
    submit: ({ email, password }) => {
      if (email && password) {
        return true
      } else {
        console.warn('Invalid submit event payload!')
        return false
      }
    }
  },
  methods: {
    submitForm(email, password) {
      this.$emit('submit', { email, password })
    }
  }
}
```