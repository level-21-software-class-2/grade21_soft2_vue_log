# ElementPlus

## Menu Attributes

| 属性名              | 说明                                                         | 类型    | 可选值                | 默认值   |
| :------------------ | :----------------------------------------------------------- | :------ | :-------------------- | :------- |
| mode                | 菜单展示模式                                                 | string  | horizontal / vertical | vertical |
| collapse            | 是否水平折叠收起菜单（仅在 mode 为 vertical 时可用）         | boolean | —                     | false    |
| ellipsis            | 是否省略多余的子项（仅在横向模式生效）                       | boolean | —                     | true     |
| background-color    | 菜单的背景颜色（十六进制格式）（已被废弃，使用`--bg-color`） | string  | —                     | #ffffff  |
| text-color          | 文字颜色（十六进制格式）（已被废弃，使用`--text-color`）     | string  | —                     | #303133  |
| active-text-color   | 活动菜单项的文本颜色（十六进制格式）（已被废弃，使用`--active-color`） | string  | —                     | #409EFF  |
| default-active      | 页面加载时默认激活菜单的 index                               | string  | —                     | —        |
| default-openeds     | 默认打开的 sub-menu 的 index 的数组                          | Array   | —                     | —        |
| unique-opened       | 是否只保持一个子菜单的展开                                   | boolean | —                     | false    |
| menu-trigger        | 子菜单打开的触发方式，只在 `mode` 为 horizontal 时有效。     | string  | hover / click         | hover    |
| router              | 是否启用 `vue-router` 模式。 启用该模式会在激活导航时以 index 作为 path 进行路由跳转 使用 `default-active` 来设置加载时的激活项。 | boolean | —                     | false    |
| collapse-transition | 是否开启折叠动画                                             | boolean | —                     | true     |
| popper-effect       | Tooltip 主题，内置了 `dark` / `light` 两种主题               | string  | dark / light          | dark     |

## 开启折叠动画

```
:collapse-transition="false"
```

```vue
<style scoped>
.el-aside {
  background-color: #545c64;
  height: calc(100vh);
  transition: width 0.25s;
  /* flex: 0 0 auto; */
}
 <style>
```

