# Element-Plus

## 包管理器

```
# 选择一个你喜欢的包管理器

# NPM
$ npm install element-plus --save

# Yarn
$ yarn add element-plus

# pnpm
$ pnpm install element-plus
```

```
npm i vue-router
```



## 用法

### 完整引入

```JS
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)
app.use(ElementPlus)
app.mount('#app')
```

<el-container>：外层容器。当子元素中包含<el-header>或<el-footer>时，内部子元素会垂直排列。针对其他元素，则会水平排列。
<el-header>：顶部栏容器。
<el-aside>：侧边栏容器。
<el-main>：内容区域容器。
<el-footer>：底部栏容器

## Icon 图标

### 使用图标

#### 安装

```
# NPM
$ npm install @element-plus/icons-vue
# Yarn
$ yarn add @element-plus/icons-vue
# pnpm
$ pnpm install @element-plus/icons-vue
```

