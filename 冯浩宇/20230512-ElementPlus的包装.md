# ElementPlus的包装

## components文件夹

### 第一步给<el-menu>加`router`

### Navbar.vue

```vue
<template>
    <div>
        <el-menu router :collapse="isCollapse" active-text-color="#ffd04b" :collapse-transition="false"
        background-color="#545c64" text-color="#fff">
            <NavbarItem :itemMenus="menus"></NavbarItem>
        </el-menu>
    </div>
</template>
<script setup>
import NavbarItem from './NavbarItem.vue'
const props = defineProps(['menus', 'isCollapse'])
</script>

<style scoped>
.el-menu {
    border-right: solid 0px #545c64;
}
</style>
```

### Layout.vue

```vue
<template>
    <div class="common-layout">
        <el-container>
            <el-aside :width="isCollapse ? '64px' : '200px'">
                <Navbar :is-collapse="isCollapse" :menus="menuList"></Navbar>
            </el-aside>
            <el-container>
                <el-header>
                    <el-icon :class="{ rotateClass: isCollapse }" :size="30" @click="iconClickHandler">
                        <fold></fold>
                    </el-icon>
                </el-header>
                <el-main>
                    <!-- <div class="inline-flex" :style="obj">
            </div> -->
                    <RouterView></RouterView>
                </el-main>
                <el-footer>
                    <el-row class="calRowTop">
                        <el-col :span="24">
                            Copyright © 2023 就爱信息有限公司. 版权所有
                        </el-col>
                    </el-row>
                    <el-row>
                        <el-col :span="24">
                            <a href="https://beian.miit.gov.cn/"
                                target="_blank">闽ICP备35065487654-1号</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="">
                                <el-image src="../public/ghs.png"></el-image>
                                闽公网安备 540654746456号</a>
                        </el-col>
                    </el-row>
                </el-footer>
            </el-container>
        </el-container>
    </div>
</template>
<script setup>
import { ref, reactive, computed } from 'vue';
import Navbar from './Navbar.vue';
import routes from '../router/routes';

const isCollapse = ref(false)

// 用于处理路由向菜单数组转换时，上下级path叠加的问题
function convertChild(arr, path) {
    let list = [];
    arr.forEach(item => {
        // 这里的菜单对象，我们抛弃了组件component属性，因为这个属性对菜单栏完全没有意义，
        let tmp = {
            path: item.path,
            meta: item.meta
        }
        // 因为传入的path不为空，所以把它叠加到子节点的path属性上
        if (path) {
            tmp.path = path + '/' + tmp.path
        }
        // 如果当前节点对象，有下级节点，并且有对象，则递归调用函数来处理下级节点
        if (item.children && item.children.length > 0) {
            let children = convertChild(item.children, item.path)
            // 经过处理的下级节点有值，则赋值给当前菜单对象
            if (children.length > 0) {
                tmp.children = children
            }
        }
        // 将处理好的菜单节点push到数组，准备返回
        list.push(tmp)
    })

    return list;
}
const menuList = computed(() => {
    let list = convertChild(routes, '')
    return list;
})

const obj = reactive({ boxShadow: 'var("--el-box-shadow-dark")', height: '30px', width: '30px', backgroundColor: 'black' })


const iconClickHandler = function () {
    console.log('888');
    isCollapse.value = !isCollapse.value
}
</script>
<style scoped>
.el-aside {
    background-color: #545c64;
    height: calc(100vh);
    transition: width 0.25s;
    /* flex: 0 0 auto; */
}

.el-header {
    background-color: red;
    display: flex;
    align-items: center;
}

.el-main {
    background-color: blue;
}

.el-footer {
    background-color: #A3A6AD;
}

.el-footer a {
    text-decoration: none;
    color: #000;
}

.el-col {
    display: flex;
    justify-content: center;
}

.rotateClass {
    transform: rotate(-180deg);
}

.calRowTop {
    margin-top: 8px;
}

.el-image {
    vertical-align: top;
}
</style>
  
```

### Navbar.veu

```vue
<template>
    <div>
        <el-menu router :collapse="isCollapse" active-text-color="#ffd04b" :collapse-transition="false"
        background-color="#545c64" text-color="#fff">
            <NavbarItem :itemMenus="menus"></NavbarItem>
        </el-menu>
    </div>
</template>
<script setup>
import NavbarItem from './NavbarItem.vue'
const props = defineProps(['menus', 'isCollapse'])
</script>

<style scoped>
.el-menu {
    border-right: solid 0px #545c64;
}
</style>
```

### Navbarltem.veu

```vue
<template>
    <!-- <div> -->
    <!-- 遍历传入的菜单数组 -->
    <template v-for="menu in itemMenus">
        <!-- 当前菜单对象如果有下级菜单，即有children属性，并且children的长度大于0，则需要使用组件中的子菜单组件el-sub-menu -->
        <el-sub-menu v-if="menu.children && menu.children.length > 0" :index="menu.path">
            <template #title>
                <el-icon>
                    <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                    <setting v-else></setting>
                </el-icon>
                <span>
                    {{ menu.meta.title }}
                </span>
            </template>
            <!-- 重复自己 -->
            <NavbarItem :item-menus="menu.children"></NavbarItem>
        </el-sub-menu>
        <el-menu-item v-else :index="menu.path">
            <el-icon>
                <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                <setting v-else></setting>
            </el-icon>
            <span>
                {{ menu.meta.title }}
            </span>

        </el-menu-item>
    </template>
    <!-- </div> -->
</template>
<script setup>
const props = defineProps(['itemMenus'])
</script>
```

## router文件夹

### index.js

```js
import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes'

const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router

```

### routes.js路由文件

```js
import Layout from '../components/Layout.vue'
const routes = [
    {
        path: '/',
        meta:{icon:'setting',title:'首页'},
        component: Layout
    },
    {
        path: '/setting',
        meta:{icon:'setting',title:'系统管理'},
        component: Layout,
        children: [
            {
                path: 'user',
                meta:{icon:'setting',title:'用户'},
                component: () => import('../views/User.vue')
            },
            {
                path: 'role',
                meta:{icon:'setting',title:'角色'},
                component: () => import('../views/Role.vue')
            },
            {
                path: 'permission',
                meta:{icon:'setting',title:'权限'},
                component: () => import('../views/Permission.vue')
            },
            // {
            //     path: 'stu',
            //     component: () => import('./views/Stu.vue')
            // },
        ]
    }
]
export default routes
```

## App.vue

```vue
<template>
  <router-view></router-view>
</template>
```

## main.js

```js
import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'


import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import router from './router'

const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(ElementPlus).use(router).mount('#app')
```

