# ElementPlus

### Navbarltem.vue

```vue
<template>
    <!-- <div> -->
        <!-- 遍历传入的菜单数组 -->
        <template v-for="menu in itemMenus">
            <!-- 当前菜单对象如果有下级菜单，即有children属性，并且children的长度大于0，则需要使用组件中的子菜单组件el-sub-menu -->
            <el-sub-menu v-if="menu.children && menu.children.length > 0" :index="menu.title">
                <template #title>
                    <el-icon>
                        <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                        <setting v-else></setting>
                    </el-icon>
                    <span>
                        {{ menu.title }}
                    </span>
                </template>
                <!-- 重复自己 -->
                <NavbarItem :item-menus="menu.children"></NavbarItem>
            </el-sub-menu>
            <el-menu-item v-else :index="menu.title">
                <el-icon>
                    <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                    <setting v-else></setting>
                </el-icon>
                <span>
                    {{ menu.title }}
                </span>

            </el-menu-item>
        </template>
    <!-- </div> -->
</template>
<script setup>
const props = defineProps(['itemMenus'])

</script>
```

### Navbar.vue

```vue
<template>
    <div>
        <el-menu :collapse="isCollapse" active-text-color="#ffd04b"
        background-color="#545c64" text-color="#fff">
            <NavbarItem :itemMenus="menus"></NavbarItem>
        </el-menu>
    </div>
</template>
<script setup>
import NavbarItem from './NavbarItem.vue'
const props = defineProps(['menus', 'isCollapse'])
</script>
<style scoped>
.el-menu {
    border-right: solid 0px #545c64;
}
</style>
```

### App.vue

```vue
<template>
  <div class="common-layout">
    <el-container>
      <el-aside :width="isCollapse ? '64px' : '200px'">
        <Navbar :is-collapse="isCollapse" :menus="menuList"></Navbar>
      </el-aside>
      <el-container>
        <el-header>
          <el-icon :class="{ rotateClass: isCollapse }" :size="30" @click="iconClickHandler">
            <fold></fold>
          </el-icon>
        </el-header>
        <el-main>
          <div class="inline-flex" :style="obj">
          </div>
        </el-main>
        <el-footer>
            <!--底部-->
        </el-footer>
      </el-container>
    </el-container>
  </div>
</template>
<script setup>
import { ref, reactive } from 'vue';
import Navbar from './components/Navbar.vue';

const isCollapse = ref(false)
const menuList = reactive([
  {
    title: '系统管理',
    meta: { icon: 'setting' },
    children: [
      {
        title: '用户管理',
        meta: { icon: 'User' }
      },
      {
        title: '角色管理',
        meta: { icon: 'Grid' }
      },
      {
        title: '权限管理',
        meta: { icon: 'Promotion' }
      },
      {
        title: '部门管理',
        meta: { icon: 'HelpFilled' },
        children: [
          {
            title: '销售部',
            meta: { icon: 'user' }
          },
          {
            title: '研发部',
            meta: { icon: 'user' }
          },
          {
            title: '财务部',
            meta: { icon: 'user' },
            children: [
              {
                title: '出纳'
              },
              {
                title: '会计',

              },
              {
                title: '财务总监'
              },
            ]
          },
        ]
      },
    ]
  }, {
    title: '学生管理',
    meta: { icon: 'setting' }
  }
])
const obj = reactive({ boxShadow: 'var("--el-box-shadow-dark")', height: '30px', width: '30px', backgroundColor: 'black' })

const iconClickHandler = function () {
  console.log('888');
  isCollapse.value = !isCollapse.value
}
</script>
<style scoped>
.el-aside {
  background-color: #545c64;
  height: calc(100vh);
}

.el-header {
  background-color: red;
  display: flex;
  align-items: center;
}

.el-main {
  background-color: blue;
}

.el-footer {
  background-color: #A3A6AD;
}
.rotateClass {
  transform: rotate(-180deg);
}
</style>

```

### main.js

```js
import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import Navbar from './components/Navbar.vue'
import Content from './components/Content.vue'

const routes = [
    {
        path: '/',
        components: {
            default: Navbar,
            cc: Content
        }
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(ElementPlus).use(router).mount('#app')
```

