# ElementPlus

## 底部样式

```vue

<template>
<el-footer>
    <el-row class="calRowTop">
      <el-col :span="24">
        Copyright © 2023 就爱信息有限公司. 版权所有
      </el-col>
    </el-row>
    <el-row>
      <el-col :span="24">
        <a href="https://beian.miit.gov.cn/" target="_blank">闽ICP备35065487654-1号</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="">
        	<el-image src="../public/ghs.png"></el-image>
        	闽公网安备 540654746456号
    	</a>
      </el-col>
    </el-row>
  </el-footer>
</template>

<style scoped>
.el-footer {
  background-color: #A3A6AD;
}

.el-footer a {
  text-decoration: none;
  color: #000;
}

.el-col {
  display: flex;
  justify-content: center;
}
.calRowTop {
margin-top: 8px;
}
.el-image {
  vertical-align: top;
}
</style>
```

