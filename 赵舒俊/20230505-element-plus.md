# element 
## 如何使用elementui
```
# NPM
$ npm install element-plus --save

# Yarn
$ yarn add element-plus

# pnpm
$ pnpm install element-plus
```
选择一条命令进行安装即可

## 快速开始

安装完成element-plus之后需要进行在main.js 的导入,以及element-plus/dis/index.css 样式的导入
```
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
let app=createApp(App);


app.use(ElementPlus)
```

# 使用element-plus 的icon 图标

## 安装
```
$ npm install @element-plus/icons-vue
# Yarn
$ yarn add @element-plus/icons-vue
# pnpm
$ pnpm install @element-plus/icons-vue
```
## 注册所有图标
您需要从 @element-plus/icons-vue 中导入所有图标并进行全局注册。

// main.ts

// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

导入所有图标

# 菜单

```js
<template>

  <el-row class="tac">
    <el-col :span="12">
      <!-- 这个是上面的标题 -->
      <el-menu
        default-active="2"
        class="el-menu-vertical-demo"
        @open="handleOpen"
        @close="handleClose"
      >

      <!-- 第一个导航栏el(elementui) sub(subject 主题) menu （菜单）  index应该是表示这个菜单是第一个的菜单-->
        <el-sub-menu index="1">
          <template #title>
            <el-icon><location /></el-icon>
            <!-- 这个是第一个子菜单的标题 -->
            <span>Navigator One</span>
          </template>
          <!-- 这是子菜单的组 group 的标题会显示在顶部 -->
          <el-menu-item-group title="Group One">  
            <el-menu-item index="1-1">item one </el-menu-item>
            <el-menu-item index="1-2">item two  o.O</el-menu-item>
          </el-menu-item-group>
          <!-- 这是菜单选项组的第一个组 -->
          
          <!-- 这是菜单选项组的第二个组 -->
          <el-menu-item-group title="Group Two">
            <el-menu-item index="1-3">item three</el-menu-item>
          </el-menu-item-group>

          <!-- 这是一个嵌套的菜单选项 -->
          <el-sub-menu index="1-4">
            <template #title>item four</template>
            <el-menu-item index="1-4-1">item one</el-menu-item>
          </el-sub-menu>
        </el-sub-menu>
        <!-- 
          <el-menu-item index="2">
          <el-icon><icon-menu /></el-icon>
          <span>Navigator Two</span>
        </el-menu-item> 
        -->
  
      </el-menu>
    </el-col>
    
  </el-row>
</template>


<script lang="ts" setup>

</script>

```

