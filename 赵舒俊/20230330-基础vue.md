use --host to expose
想要让其他局域网内的主机进行访问你创建vue文件，需要进行开启访问。
第一种方法是在export default defineconfig 中进行配置 server{ host:0.0.0.0}
第二种方法是（通过 Vite CLI 配置）在执行 npx vite --host 0.0.0.0 命令后，就可以通过 http://10.56.116.128:3000/ 访问了
第三种方式是修改npm脚本 其中就是第二种方法将yarn dev 的执行命令变成第二种方法的形式


```js
import { ref } from 'vue'
// 从helloworld 中可以看出setup 不仅能进行配置导入的内容还能进行配置导出的内容

// 定义属性
defineProps({
  msg: String,
  name:String,
})

const count = ref(0)
```
ref是用于数据的双向绑定这里的0表示定义一个初始值

<HelloWorld msg="konqiao" name="zhangsan"/>
就相当于导入helloworld中所有的东西，然后后面是给他进行赋值。

 <span v-html="rawHtml"></span></p>
 const rawHtml = '<span style="color: red">This should be red.</span>'
 就是在v-html 直接进行绑定

# nohup
你可以使用以下命令在 Debian 服务器上以后台方式启动一个程序：

nohup command &
其中，command 是你要运行的程序的命令，& 表示在后台运行程序。nohup 命令可以让程序在退出终端后继续运行，并且将程序的输出重定向到一个名为 nohup.out 的文件中。

例如，如果你要以后台方式启动一个名为 app.js 的 Node.js 应用程序，你可以使用以下命令：

bash
Copy code
nohup node app.js &
这将在后台启动该应用程序，并将输出重定向到 nohup.out 文件中。你可以使用 tail 命令来查看该文件的内容，以了解程序的运行情况：

bash
Copy code
tail -f nohup.out


