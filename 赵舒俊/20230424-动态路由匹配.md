
```js
<script setup>
import { ref } from 'vue'

const emit2=defineEmits(["aaa"])

const asdfTest=function(){
  emit2("aaa")
}

</script>

<template>
  <button @click="asdfTest">asdf</button>
</template>

```
defineEmits 返回的对象能够进行发送


# 路由

```js

<p>
   <router-link to="/home">Go to Home</router-link>
    <router-link to="/about">Go to About</router-link>
  </p>
  
  <!-- 路由出口 -->
  <!-- 路由匹配到的组件将渲染在这里 -->
  <router-view></router-view>
```
router-link 有点类似a标签，但是router-link是对路由进行操作。


router-view 表示路由渲染到这个位置。


## 动态匹配
```js

const routes = [
  { path: '/home', component: Home },
  { path: '/about/:id', component: About },
]

```
/about/:id 会将http://localhost:5173/#/about/me  和 http://localhost:5173/#/about/1 
都会进行匹配
  <h1>about{{ $route.params.id }}</h1>
  可以进行动态参数的拿取


## 历史模式

在创建路由器实例时，history 配置允许我们在不同的历史模式中进行选择。

Hash 模式
hash 模式是用 createWebHashHistory() 创建的：

js
import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    //...
  ],
})
它在内部传递的实际 URL 之前使用了一个哈希字符（#）。由于这部分 URL 从未被发送到服务器，所以它不需要在服务器层面上进行任何特殊处理。不过，它在 SEO 中确实有不好的影响。如果你担心这个问题，可以使用 HTML5 模式。

HTML5 模式
用 createWebHistory() 创建 HTML5 模式，推荐使用这个模式：

js
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    //...
  ],
})
当使用这种历史模式时，URL 会看起来很 "正常"，例如 https://example.com/user/id。漂亮!

不过，问题来了。由于我们的应用是一个单页的客户端应用，如果没有适当的服务器配置，用户在浏览器中直接访问 https://example.com/user/id，就会得到一个 404 错误。这就尴尬了。

不用担心：要解决这个问题，你需要做的就是在你的服务器上添加一个简单的回退路由。如果 URL 不匹配任何静态资源，它应提供与你的应用程序中的 index.html 相同的页面。漂亮依旧!
