# 事件参数​
有时候我们会需要在触发事件时附带一个特定的值。举例来说，我们想要 <BlogPost> 组件来管理文本会缩放得多大。在这个场景下，我们可以给 $emit 提供一个额外的参数：

template
<button @click="$emit('increaseBy', 1)">
  Increase by 1
</button>
然后我们在父组件中监听事件，我们可以先简单写一个内联的箭头函数作为监听器，此函数会接收到事件附带的参数：

template
<MyButton @increase-by="(n) => count += n" />
或者，也可以用一个组件方法来作为事件处理函数：

template
<MyButton @increase-by="increaseCount" />
该方法也会接收到事件所传递的参数：

js
function increaseCount(n) {
  count.value += n
}

## 事件参数
可以传递一些额外的参数

```js
 <button @click="$emit('someEvent',1)">click me </button>

 const count=ref(1);
const helloEvent=function(aaa){
  console.log("hhhhh");
  count.value+=aaa
  console.log(count.value);
  
}

  <HelloWorld  @someEvent="helloEvent"/>
```

## defineEmits
定义事件emit前需要先进行定义，不然会出警告
```js
<script setup >

const emit=defineEmits(['inFocus','submit','someEvent'])

const someEvent=function(){
  emit('someEvent',1)
}
</script>

<template>
  
  <button @click="someEvent">click me </button>
  <!-- MyComponent
<button @click="$emit('someEvent')">click me</button> -->

</template>

<style scoped>
.read-the-docs {
  color: #888;
}
</style>

```

对象类型定义
```js
<script setup >


let emit=defineEmits({ submit: ({ email, password }) => {
    if (email && password) {
      return true
    } else {
      console.warn('Invalid submit event payload!')
      return false
    }
  }
  ,someEvent:null
}
)

const someEvent=function(){
  emit('someEvent',1)
}
</script>

<template>
  
  <button @click="someEvent">click me </button>
  <!-- MyComponent
<button @click="$emit('someEvent')">click me</button> -->

</template>

<style scoped>
.read-the-docs {
  color: #888;
}
</style>

```

## 事件校验

```js
<script setup >


let emit=defineEmits({ submit: ({ email, password }) => {
    if (email && password) {
      console.log("successful");
      return true
    } else {
      console.warn('Invalid submit event payload!')
      return false
    }
  }
  ,someEvent:null
}
)

const someEvent=function(email,password){
  emit('submit',{email,password})
}
</script>

<template>

  <button @click="someEvent(1)">click me </button>


</template>

```
可以进行定义事件参数校验，需要定义defineEmits 的时候传递对象类型，然后进行校验。
