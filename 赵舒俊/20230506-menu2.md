<script setup>
import { useRouter } from 'vue-router'

defineProps(["elMenuItem"])
const router = useRouter()
const menuItemClick = function (title) {
    switch (title) {
        case "增加员工":
            router.push("/addUser");
            break;
        case "增加商品":
            router.push("/addGoods");
            break;
        case "增加员工":
            router.push("/addUser");
            break;
    }

}
</script>

<template >
    <template v-for="item in elMenuItem">

        <el-sub-menu v-if="item.children && item.children.length > 0" :index=item.key>
            <template #title>
                <el-icon>
                    <component :is=item.icon></component>
                </el-icon>
                {{ item.title }}
            </template>
            <leftNavItem :elMenuItem=item.children />
        </el-sub-menu>
        <el-menu-item v-else @click="menuItemClick(item.title)">
            {{ item.title }}
        </el-menu-item>
    </template>
</template>