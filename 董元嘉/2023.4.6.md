计算属性
基础示例​
模板中的表达式虽然方便，但也只能用来做简单的操作。如果在模板中写太多逻辑，会让模板变得臃肿，难以维护。

<script >
export default {
  data() {
    return {
      stu:{
        id:1,
        score:
        {
          chinese:120,
          english:90,
          math:110
        }
      }
    }
  },
  computed:{
    countScore(){
      let cc = 0;
      let score = this.stu.score;
      for(let key in this.stu.score){
        if (score[key]>0) {
          cc += 1;
        }
      }
      return cc;
    }
  }
}
</script>
  
<template>
<p>
    有{{ countScore }}门成绩合格
</p>
</template>


计算属性缓存 vs 方法
若我们将同样的函数定义为一个方法而不是计算属性，两种方式在结果上确实是完全相同的，然而，不同之处在于计算属性值会基于其响应式依赖被缓存。一个计算属性仅会在其响应式依赖更新时才重新计算。这意味着只要 score 不改变，无论多少次访问 countScore 都会立即返回先前的计算结果，而不用重复执行 getter 函数。

为什么需要缓存呢？想象一下我们有一个非常耗性能的计算属性 list，需要循环一个巨大的数组并做许多计算逻辑，并且可能也有其他计算属性依赖于 list。没有缓存的话，我们会重复执行非常多次 list 的 getter，然而这实际上没有必要！如果你确定不需要缓存，那么也可以使用方法调用。

最佳实践​
Getter 不应有副作用​
计算属性的 getter 应只做计算而没有任何其他的副作用，这一点非常重要，请务必牢记。举例来说，不要在 getter 中做异步请求或者更改 DOM！一个计算属性的声明中描述的是如何根据其他值派生一个值。因此 getter 的职责应该仅为计算和返回该值。在之后的指引中我们会讨论如何使用监听器根据其他响应式状态的变更来创建副作用。

类与样式绑定

数据绑定的一个常见需求场景是操纵元素的 CSS class 列表和内联样式。因为 class 和 style 都是 attribute，我们可以和其他 attribute 一样使用 v-bind 将它们和动态的字符串绑定。但是，在处理比较复杂的绑定时，通过拼接生成字符串是麻烦且易出错的。因此，Vue 专门为 class 和 style 的 v-bind 用法提供了特殊的功能增强。除了字符串外，表达式的值也可以是对象或数组。
绑定 HTML class​

绑定对象
我们可以给 :class (v-bind:class 的缩写) 传递一个对象来动态切换 class：

<script >
export default {
 data(){
  return {
    isxyz:true
  } 
 }
}
</script>
  
<template>
  <p :class="{xyz:isxyz}">光年之外</p>
</template>

<style scoped>
.xyz{
  color: red;
}
</style>

绑定数组
我们可以给 :class 绑定一个数组来渲染多个 CSS class：

<script >
export default {
  data(){
    return {
      isa:'act',
      err:'test',
      yyobj:{
        abc:true,
        xyz:false
      },
      ccArr:['123456',
        {
          dis:true
        },{
          err:false
        }
      ]
    }
  }
}
</script>
  
<template>
  <p :class="ccArr">光年之外</p>
</template>

绑定内联样式style
绑定对象
:style 支持绑定 JavaScript 对象值，对应的是 HTML 元素的 style 属性

<template>
  <p :style="{ fontSize:'180px' }">光年之外</p>
</template>

绑定数组
我们还可以给 :style 绑定一个包含多个样式对象的数组。这些对象会被合并后渲染到同一元素上

<div :style="[baseStyles, overridingStyles]"></div>