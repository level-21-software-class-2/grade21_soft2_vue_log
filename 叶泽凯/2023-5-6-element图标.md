# 图标
* 安装
```js
NPM
npm install @element-plus/icons-vue

Yarn
yarn add @element-plus/icons-vue

pnpm
pnpm install @element-plus/icons-vue
```
* 写入main.js
```js
// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
```