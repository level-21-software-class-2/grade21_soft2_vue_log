# 20230413--vue生命周期
vue生命周期，就像人生一样的短暂，充满着未知的情况，从一开始的出生，小孩，少年，青年，中年，晚年，

知道死亡的那刻来临，每一个阶段都会处理不同的事情，人的整个生涯就算是走完。

**Vue实例也一样有着一个完整的生命周期，也就是说从开始创建，初始化数据，编译模板，挂载DOM，渲染-更新-渲染，卸载等一系列过程，构成为Vue实例的生命周期，钩子就是在某个阶段做一些处理**。
## 生命周期钩子
每个 Vue 组件实例在创建时都需要经历一系列的初始化步骤，比如设置好数据侦听，编译模板，挂载实例到 DOM，以及在数据改变时更新 DOM。在此过程中，它也会运行被称为生命周期钩子的函数，让开发者有机会在特定阶段运行自己的代码。
## 注册周期钩子
举例来说，`mounted` 钩子可以用来在组件完成初始渲染并创建 DOM 节点后运行代码：
js
```
export default {
  mounted() {
    console.log(`the component is now mounted.`)
  }
}
```
还有其他一些钩子，会在实例生命周期的不同阶段被调用，

最常用的是 [`mounted`](https://cn.vuejs.org/api/options-lifecycle.html#mounted)、

[`updated`](https://cn.vuejs.org/api/options-lifecycle.html#updated) 和 

[`unmounted`](https://cn.vuejs.org/api/options-lifecycle.html#unmounted)。

所有生命周期钩子函数的 `this` 上下文都会自动指向当前调用它的组件实例。

注意：避免用箭头函数来定义生命周期钩子，因为如果这样的话你将无法在函数中通过 `this` 获取组件实例。