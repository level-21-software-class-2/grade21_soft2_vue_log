## 具名插槽
### helloworld.vue
```
<template>
  <div>
  <slot name="head">
  </slot>
  <slot name="foot">
  </slot>
  </div>
</template>
```
### App.vue
```
<template>
  <HelloWorld>
    <template #head>
      <p>123</p>
    </template>
    <template #foot>
      <p>456</p>
    </template>
  </HelloWorld>
</template>
```
### 效果图
[![p9PxvwV.jpg](https://s1.ax1x.com/2023/04/18/p9PxvwV.jpg)](https://imgse.com/i/p9PxvwV)
## 全局注册
### main.js
```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Test from './components/Test.vue'
createApp(App).component('test',Test).mount('#app')
```
### Test.vue
```
<template>
    111
</template>
```
### HelloWorld.vue
```
<template>
<test></test>
</template>
```
### App.vue
```
<template>
  <HelloWorld/>
</template>
```
## 动态组件
### App.vue
```
<script>
import HelloWorld from './components/HelloWorld.vue'
import Test from './components/Test.vue'
export default {
  components:{
    HelloWorld,
    Test
  },
  data() {
    return {
      Btn:'HelloWorld'
    }
  },
  methods: {
    change() {
      this.Btn='Test'
    }
  }
}
</script>

<template>
  <component :is="Btn"></component>
  <input type="button" value="确定" @click="change">
</template>
```
### HelloWorld.vue
```
<template>
222
</template>
```
### Test.vue
```
<template>
    111
</template>
```