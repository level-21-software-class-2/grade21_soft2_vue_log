## 一.设置路由的匹配

+ 进入路由配置文件，在定义的路由中添加相关的路径信息

### 带参路由的动态匹配

+ 路径参数 用**冒号 :** 表示。当一个路由被匹配时，它的 params 的值将在每个组件中以 **this.$route.params** 的形式暴露出来。

+ **component** 获取要跳转页面的位置

```js
//2.定义路由
const routes = [
    { path: '/student/:id', component: StudentId },
]
```

### 错误路由的匹配

+ 在括号之间使用了自定义正则表达式，并将 **pathMatch** 参数标记为可选可重复。

+ 在 to 中输入 **/:pathMatch(.*)** 匹配错误的路径信息

+ + **component** 获取要跳转页面的位置

```js
//2.定义路由
const routes = [
    //将匹配不到的报错页面，跳转为此页面
    { path: '/:pathMatch(.*)', component: NotFound }
]
```

#
## 二.获取路由信息

+ **vue2**路由是通过this.$route和this.$router来控制的

+ **vue3**的路由中

    + **useRoute** 相当于以前的 **this.$route**，

    + **useRouter** 相当于 **this.$router**

### $route

+ **route** 是一个跳转的路由对象，每一个路由都会有一个route对象

+ **route** 是一个**局部的对象**，可以获取对应的name,path,params,query等

    + **$route.path** :获取当前路由对象的路径，会被解析为绝对路径，如 “/index/” 。

    + **$route.params** ：获取包含路由中的动态片段和全匹配片段的键值对

    + **$route.query** ：获取 **? 号传参**包含路由中查询参数的键值对。例如，对于 /index?id=1 ，会得到 $route.query.id == 1。
```js
    { path: '/student/:id', component: StudentId },
```
```html
<h1>这里是id为{{ $route.params.id}} 的页面</h1>
```


### userRoute

+ 相当于 $route

+ **push方法** 是useRouter()才有的, 用**useRoute()实例化**的对象是没这方法的
```js
//实例化
const route=userRoute()
```

### $router

+ **router** 是VueRouter的一个对象，通过Vue.use(VueRouter)和VueRouter构造函数得到一个router的实例对象

+ **router** 是一个**全局的对象**，包含了所有的路由包含了许多关键的对象和属性。例如history对象

    + **$router.push({path:’/path’})** : 本质是向history栈中添加一个路由，在我们看来是 切换路由，但本质是在添加一个history记录

    + **$router.replace({path:’/path’})** : 替换路由，没有历史记录

### userRouter

+ 相当于 $router 

+ 通过useRouter来手动控制路由变化

+ push方法是useRouter()才有的

```js
//实例化
const router=userRouter()
```
