# 5.4笔记
# ElementPlus
## 安装ElementPlus
```JS
# NPM
$ npm install element-plus --save

# Yarn
$ yarn add element-plus

# pnpm
$ pnpm install element-plus
```
## 引入
```JS
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

createApp(App).use(ElementPlus).mount('#app')
```
## 布局高度自适应
```JS
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="200px">Aside</el-aside>
      <el-container>
        <el-header>Header</el-header>
        <el-main>Main</el-main>
        <el-footer>Footer</el-footer>
      </el-container>
    </el-container>
  </div>
</template>


<style scoped>

.el-aside{
  background-color: #f3d19e;
  height: 100vh;
}

.el-header{
  background-color: #fcd3d3;
}

.el-main{
  background-color: #dedfe0;
}

.el-footer{
  background-color: #d1edc4;
}
</style>
```