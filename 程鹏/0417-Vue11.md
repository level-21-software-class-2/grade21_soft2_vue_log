组件注册​
此章节假设你已经看过了组件基础。若你还不了解组件是什么，请先阅读该章节。

一个 Vue 组件在使用前需要先被“注册”，这样 Vue 才能在渲染模板时找到其对应的实现。组件注册有两种方式：全局注册和局部注册。

全局注册​
我们可以使用 Vue 应用实例的 app.component() 方法，让组件在当前 Vue 应用中全局可用。

js
import { createApp } from 'vue'

const app = createApp({})

app.component(
  // 注册的名字
  'MyComponent',
  // 组件的实现
  {
    /* ... */
  }
)