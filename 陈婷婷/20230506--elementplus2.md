## main.js
```js
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { Menu as IconMenu, Message, Setting } from '@element-plus/icons-vue'

const app = createApp(App)

app.use(ElementPlus)

app.use(ElementPlusIconsVue)

app.mount('#app')
```

## app.vue
```js
<template>
  <el-container class="layout-container-demo" style="height: 500px">
    <el-aside width="300px">
      <el-scrollbar>
        <el-menu :default-openeds="['1', '3']">
          <el-sub-menu index="1">
            <template #title>
              <el-icon><UserFilled /></el-icon>用户管理
            </template>
            <el-sub-menu >
              <template #title>账号管理</template>
              <el-menu-item index="1-1-1">验证</el-menu-item>
              <el-menu-item index="1-1-2">注册</el-menu-item>
              <el-menu-item index="1-1-3">第三方授权</el-menu-item>
              <el-menu-item index="1-1-4">权限</el-menu-item> 
            </el-sub-menu>
            <el-sub-menu index="1-2">
              <template #title>信息管理</template>
              <el-menu-item index="1-2-31">地理位置 </el-menu-item> 
              <el-sub-menu index="1-2-2">
              <template #title>属性</template>
              </el-sub-menu>
              <el-menu-item index="1-2-3">设备</el-menu-item> 
              <el-menu-item index="1-2-4">日志</el-menu-item> 

            </el-sub-menu>
          </el-sub-menu>
          <el-sub-menu index="2">
            <template #title>
              <el-icon><icon-menu /></el-icon>系统管理
            </template>
              <el-sub-menu index="2-1">
              <template #title>部门管理</template>
              <el-sub-menu index="2-1-1">
                <template #title>技术部</template>
                <el-menu-item index="2-1-1-1">初级工程师</el-menu-item>
                <el-menu-item index="2-1-1-1">中级工程师</el-menu-item>
                <el-menu-item index="2-1-1-1">高级工程师</el-menu-item>
              </el-sub-menu>
              <el-sub-menu index="2-1-2">
                <template #title>财务部</template>
                  <el-sub-menu index="2-1-2-1">
                    <template #title>会计</template>
                    <el-menu-item index="2-1-2-1-1">初级会计</el-menu-item> 
                    <el-menu-item index="2-1-2-1-2">中级会计</el-menu-item> 
                    <el-menu-item index="2-1-2-1-3">高级会计</el-menu-item>   
                  </el-sub-menu>
                  <el-menu-item index="2-1-2-2">出纳</el-menu-item> 
                  <el-menu-item index="2-1-2-3">财务总监</el-menu-item> 
              </el-sub-menu>
              <el-menu-item index="2-1-3">客服部</el-menu-item> 
              <el-sub-menu index="2-1-4">
                <template #title>行政部</template>
                <el-menu-item index="2-1-4-1">办公室主任</el-menu-item> 
                <el-menu-item index="2-1-4-2">行政文员</el-menu-item> 
                <el-menu-item index="2-1-4-3">行政总监</el-menu-item> 
                <el-menu-item index="2-1-4-4">行政经理</el-menu-item> 
                <el-menu-item index="2-1-4-5">行政助理</el-menu-item> 
              </el-sub-menu>
              <el-menu-item index="2-1-5">推广部</el-menu-item>
            </el-sub-menu>
              <el-sub-menu index="2-2">
                <template #title>项目管理</template>
                <el-sub-menu index="2-2-1">
                  <template #title>新建项目</template>
                  <el-menu-item index="2-2-1-1">《坤你太美实在》</el-menu-item> 
                <el-menu-item index="2-2-1-2">《泰酷辣给你大拇哥》</el-menu-item> 
                <el-menu-item index="2-2-1-3">《诞生于1996》</el-menu-item> 
                </el-sub-menu>
                <el-sub-menu index="2-2-2">
                  <template #title>扩建项目</template>
                  <el-menu-item index="2-2-1-3">《泰裤辣之你能吗》</el-menu-item>
                </el-sub-menu>
                <el-sub-menu index="2-2-3">
                  <template #title>改建项目</template>
                  <el-menu-item index="2-2-1-3">《我是一个固执的人》</el-menu-item>
                </el-sub-menu>
                <el-sub-menu index="2-2-4">
                  <template #title>迁建项目</template>
                  <el-menu-item index="2-2-1-3">《坤靠近就融化》</el-menu-item>
                </el-sub-menu>
                <el-sub-menu index="2-2-5">
                  <template #title>重建项目</template>
                  <el-menu-item index="2-2-1-3">《坤一眼就爆炸》</el-menu-item> 
                </el-sub-menu>
              </el-sub-menu>
          </el-sub-menu>
          <el-sub-menu index="3">
            <template #title>
              <el-icon><HelpFilled /></el-icon>资源管理
            </template>
            <el-sub-menu index="3-1">
              <template #title>人力资源规划</template>
            </el-sub-menu>
            <el-sub-menu index="3-2">
              <template #title>招聘配置</template>
            </el-sub-menu>
            <el-sub-menu index="3-3">
              <template #title>培训开发</template>
            </el-sub-menu>
            <el-sub-menu index="3-4">
              <template #title>薪酬</template>
            </el-sub-menu>
            <el-sub-menu index="3-5">
              <template #title>绩效</template>
            </el-sub-menu>
            <el-sub-menu index="3-6">
              <template #title>劳动关系</template>
            </el-sub-menu>
          </el-sub-menu>
        </el-menu>
      </el-scrollbar>
    </el-aside>

    <el-container>
      <el-header style="text-align: right; font-size: 12px">
      </el-header>
    </el-container>
  </el-container>
</template>

<script lang="ts" setup>
import { ref } from 'vue'
import { Menu as IconMenu, UserFilled, Message, Setting , HelpFilled} from '@element-plus/icons-vue'

const item = {

}
const tableData = ref(Array.from({ length: 20 }).fill(item))
</script>

<style scoped>
.layout-container-demo .el-header {
  position: relative;
  background-color: var(--el-color-primary-light-7);
  color: var(--el-text-color-primary);
}
.layout-container-demo .el-aside {
  color: var(--el-text-color-primary);
  background: var(--el-color-primary-light-8);
  height: 100vh;
}
.layout-container-demo .el-menu {
  border-right: none;
}
.layout-container-demo .el-main {
  padding: 0;
}
.layout-container-demo .toolbar {
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  right: 20px;
}
</style>
```