# 路由匹配语法
## 在参数中自定义正则
```js
const routes = [
  // /:orderId -> 仅匹配数字
  { path: '/:orderId(\\d+)' },
  // /:productName -> 匹配其他任何内容
  { path: '/:productName' },
]
```
*(匹配0个或多个路由)  
+(匹配1个或多个路由)
# 嵌套路由
嵌套路由关键字children  
main.js
```js
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import {createRouter,createWebHistory} from 'vue-router'
import Hello from './components/HelloWorld.vue'
import one from './components/Helloone.vue'
import two from './components/Hellotwo.vue'

const routes = [
    {path:'/user/:id',component:Hello,children:[{path:'hi',component:one},{path:'hello',component:two}]}
]

const router = createRouter({
    history:createWebHistory(),
    routes
})

const app = createApp(App);

app.use(router);

app.mount('#app')
```