# Props

## Props 声明
一个组件需要显示声明它所接受的props，这样Vue才能知道为外部传入的哪些是Props，哪些是透传attribute

props需要使用pros选项来定义：
```js
export default {
  props: ['foo'],
  created() {
    // props 会暴露到 `this` 上
    console.log(this.foo)
  }
}
```

除了使用字符串数组来声明 prop 外，还可以使用对象的形式：

```js
export default {
  props: {
    title: String,
    likes: Number
  }
}
```

对象形式的 props 声明不仅可以一定程度上作为组件的文档，而且如果其他开发者在使用你的组件时传递了错误的类型，也会在浏览器控制台中抛出警告


# 传递prop的细节
