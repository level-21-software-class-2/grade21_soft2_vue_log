# 底部
## el-footer
```vue
<el-footer>

            <el-row>
              <el-col :span="12">
                <div class="mar1">
                  Copyright ©2023 让我康康商标专利事务所 All Rights Reserved.
                </div>
              </el-col>
              <el-col :span="12">
                <div class="mar1">
                  <img src="https://bkssl.bdimg.com/static/wiki-common/widget/component/footer/img/icon-police_5f07082.png" alt="">
                  <a href="/" id="aaa">粤ICP备20057364号</a>
                </div>
              </el-col>
            </el-row>
            <el-row>
              <el-col :span="24">
                <div class="top">
                  声明：转载内容版权归作者及来源网站所有，本站原创内容转载请注明来源。
                </div>
              </el-col>
            </el-row>
          </el-footer>
```