### Vue  
##### vue-cli
```js
vue -V  // 查询vue-cli版本 
npm uninstall -g vue-cli  //卸载旧版vue-cli

npm install -g @vue/cli // 安装vue-cli3/cli4版本（本文选此项）
npm install -g vue-cli // 安装vue-cli2版本
复制代码
其他常用命令：
vue -V  // 查询vue-cli脚手架版本 
npm list vue  // 查询vue版本 
npm -V  // 查询npm版本 
node -v  // 查询node版本
nvm ls  //查询已安装的node版本，需先安装nvm
```
##### 创建项目
```js
vue create vue3-demo // vue-cli3/cli4版本 （本文选此项）
vue init webpack vue2-demo // vue-cli2版本
```
##### 运行
```js
npm run serve
```
#### 文本插值
```vue
<div id="app">
  <p>{{ message }}</p>
</div>
<div id="example1" class="demo">
    <p>使用双大括号的文本插值: {{ rawHtml }}</p>
    <p>使用 v-html 指令: <span v-html="rawHtml"></span></p>
</div>
'<span style="color: red">这里会显示红色！</span>'
```
###### v-bind
HTML 属性中的值应使用 v-bind 指令。

<div v-bind:id="dynamicId"></div>