# 模板引用
虽然 Vue 的声明性渲染模型为你抽象了大部分对 DOM 的直接操作，但在某些情况下，我们仍然需要直接访问底层 DOM 元素。要实现这一点，我们可以使用特殊的 `ref attribute`：
```js
<input ref="input">
```
`ref` 是一个特殊的 `attribute`，和 `v-for` 章节中提到的 `key` 类似。它允许我们在一个特定的 DOM 元素或子组件实例被挂载后，获得对它的直接引用。这可能很有用，比如说在组件挂载时将焦点设置到一个 `input` 元素上，或在一个元素上初始化一个第三方库。

## 访问模板引用
挂载结束后引用都会被暴露在 `this.$refs` 之上：
```vue
<script>
export default {
  mounted() {
    this.$refs.input.focus()
  }
}
</script>

<template>
  <input ref="input" />
</template>
```
注意，你只可以在组件挂载后才能访问模板引用。如果你想在模板中的表达式上访问 `$refs.input`，在初次渲染时会是 null。这是因为在初次渲染前这个元素还不存在呢！

## v-for 中的模板引用
当在 v-for 中使用模板引用时，相应的引用中包含的值是一个数组：
```vue
<script>
export default {
  data() {
    return {
      list: [
        /* ... */
      ]
    }
  },
  mounted() {
    console.log(this.$refs.items)
  }
}
</script>

<template>
  <ul>
    <li v-for="item in list" ref="items">
      {{ item }}
    </li>
  </ul>
</template>
```
应该注意的是，ref 数组并不保证与源数组相同的顺序。

# 组件基础
## 定义一个组件
当使用构建步骤时，我们一般会将 Vue 组件定义在一个单独的 .vue 文件中，这被叫做单文件组件 (简称 SFC)：
```vue
<script>
export default {
  data() {
    return {
      count: 0
    }
  }
}
</script>

<template>
  <button @click="count++">You clicked me {{ count }} times.</button>
</template>
```
当不使用构建步骤时，一个 Vue 组件以一个包含 Vue 特定选项的 JavaScript 对象来定义
```vue
export default {
  data() {
    return {
      count: 0
    }
  },
  template: `
    <button @click="count++">
      You clicked me {{ count }} times.
    </button>`
}
```

## 使用组件
```vue
<script setup>//组合式
import HelloWorld from './components/HelloWorld.vue'
</script>

<script>//选项式
import HelloWorld from './components/HelloWorld.vue'
export default {
  components: {
    HelloWorld
  }
}
</script>
```

## 传递 props
props是属性的缩写
```vue
<script>
export default {
  props: ['title']
}
</script>

<template>
  <h1>{{ title }}</h1>
</template>

```
```vue
<HelloWorld title="123"></HelloWorld>
```

### v-for循环参传
```vue
<script >
import HelloWorld from './components/HelloWorld.vue'
export default {
  components:{
    HelloWorld
  },
  data(){
    return {
      list:[
        {
          id:1,
          title:'root',
          author:'123456'
        },
        {
          id:2,
          title:'user',
          author:'66666'
        }
      ]
    }
  }
}
</script>

<template>
  <HelloWorld />
  <HelloWorld v-for="item in list" :title="item.title" :author="item.author"></HelloWorld>
</template>
```
####  函数模板引用（ref）

<input :ref="(el) => { /* 将 el 赋值给一个数据属性或 ref 变量 */ }">
