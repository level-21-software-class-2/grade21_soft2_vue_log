## 声明响应式状态​
选用选项式 API 时，会用 data 选项来声明组件的响应式状态。此选项的值应为返回一个对象的函数。Vue 将在创建新组件实例的时候调用此函数，并将函数返回的对象用响应式系统进行包装。此对象的所有顶层属性都会被代理到组件实例 (即方法和生命周期钩子中的 this) 上。

js
export default {
  data() {
    return {
      count: 1
    }
  },

  // `mounted` 是生命周期钩子，之后我们会讲到
  mounted() {
    // `this` 指向当前组件实例
    console.log(this.count) // => 1

    // 数据属性也可以被更改
    this.count = 2
  }
}

这些实例上的属性仅在实例首次创建时被添加，因此你需要确保它们都出现在 data 函数返回的对象上。若所需的值还未准备好，在必要时也可以使用 null、undefined 或者其他一些值占位。

虽然也可以不在 data 上定义，直接向组件实例添加新属性，但这个属性将无法触发响应式更新。

Vue 在组件实例上暴露的内置 API 使用 $ 作为前缀。它同时也为内部属性保留 _ 前缀。因此，你应该避免在顶层 data 上使用任何以这些字符作前缀的属性。

## 注册周期钩子​
举例来说，mounted 钩子可以用来在组件完成初始渲染并创建 DOM 节点后运行代码：

js
export default {
  mounted() {
    console.log(`the component is now mounted.`)
  }
}
还有其他一些钩子，会在实例生命周期的不同阶段被调用，最常用的是 mounted、updated 和 unmounted。

所有生命周期钩子函数的 this 上下文都会自动指向当前调用它的组件实例。注意：避免用箭头函数来定义生命周期钩子，因为如果这样的话你将无法在函数中通过 this 获取组件实例。

### 生命周期图

![](Picture%20File/%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%9B%BE.JPG)

## 列表渲染​
v-for​
我们可以使用 v-for 指令基于一个数组来渲染一个列表。v-for 指令的值需要使用 item in items 形式的特殊语法，其中 items 是源数据的数组，而 item 是迭代项的别名：

js
data() {
  return {
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}
template
<li v-for="item in items">
  {{ item.message }}
</li>
在 v-for 块中可以完整地访问父作用域内的属性和变量。v-for 也支持使用可选的第二个参数表示当前项的位置索引。

js
data() {
  return {
    parentMessage: 'Parent',
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}
template
<li v-for="(item, index) in items">
  {{ parentMessage }} - {{ index }} - {{ item.message }}
</li>
Parent - 0 - Foo
Parent - 1 - Bar

v-for 变量的作用域和下面的 JavaScript 代码很类似：

js
const parentMessage = 'Parent'
const items = [
  /* ... */
]

items.forEach((item, index) => {
  // 可以访问外层的 `parentMessage`
  // 而 `item` 和 `index` 只在这个作用域可用
  console.log(parentMessage, item.message, index)
})
注意 v-for 是如何对应 forEach 回调的函数签名的。实际上，你也可以在定义 v-for 的变量别名时使用解构，和解构函数参数类似：

template
<li v-for="{ message } in items">
  {{ message }}
</li>

<!-- 有 index 索引时 -->
<li v-for="({ message }, index) in items">
  {{ message }} {{ index }}
</li>
对于多层嵌套的 v-for，作用域的工作方式和函数的作用域很类似。每个 v-for 作用域都可以访问到父级作用域：

template
<li v-for="item in items">
  <span v-for="childItem in item.children">
    {{ item.message }} {{ childItem }}
  </span>
</li>
你也可以使用 of 作为分隔符来替代 in，这更接近 JavaScript 的迭代器语法：

template
<div v-for="item of items"></div>

## 表单输入绑定​
在前端处理表单时，我们常常需要将表单输入框的内容同步给 JavaScript 中相应的变量。手动连接值绑定和更改事件监听器可能会很麻烦：

template
<input
  :value="text"
  @input="event => text = event.target.value">
v-model 指令帮我们简化了这一步骤：

template
<input v-model="text">
另外，v-model 还可以用于各种不同类型的输入，<textarea>、<select> 元素。它会根据所使用的元素自动使用对应的 DOM 属性和事件组合：

文本类型的 <input> 和 <textarea> 元素会绑定 value property 并侦听 input 事件；
<input type="checkbox"> 和 <input type="radio"> 会绑定 checked property 并侦听 change 事件；
<select> 会绑定 value property 并侦听 change 事件。

### 基本用法​
文本​
template
<p>Message is: {{ message }}</p>
<input v-model="message" placeholder="edit me" />