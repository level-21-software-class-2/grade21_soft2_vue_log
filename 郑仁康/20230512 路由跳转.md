## 使用路由跳转
很多情况下，我们在执行点击按钮跳转页面之前还会执行一系列方法，这时可以使用 this.$router.push(location) 来修改 url，完成跳转。

push 后面可以是对象，也可以是字符串：

// 字符串
this.$router.push('/home/first')
// 对象
this.$router.push({ path: '/home/first' })
// 命名的路由
this.$router.push({ name: 'home', params: { userId: wise }})
跳转页面并传递参数的方法：

## 1.Params

由于动态路由也是传递params的，所以在 this.$router.push() 方法中path不能和params一起使用，否则params将无效。需要用name来指定页面。

及通过路由配置的name属性访问

在路由配置文件中定义参数：

/* router.js 文件*/
import Vue from "vue";
import Router from "vue-router";
import MediaSecond from "@/views/EnterprisePage/MediaMatrix/second"; //资讯列表
 
Vue.use(Router);
export default new Router({
  routes: [ /* 进行路由配置 */
    {
        name: "MediaSecond",
        path: "/MediaSecond",
        component: MediaSecond
    },
  ]
})
 
/* 后面还需要接一空行，否则无法通过 ESlint 语法验证 */
通过name获取页面，传递params：

this.$router.push({ name: 'MediaSecond',params:{artistName:artistName,imgUrl:imgUrl,type:2} })
在目标页面通过this.$route.params获取参数：

if (this.$route.params.type == 2) {
    this.type = apis.getAtistDetails;
} else {
    this.type = apis.getMessageList;
}
## 2.Query

页面通过path/name和query传递参数，该实例中row为某行表格数据

this.$router.push({ name: 'DetailManagement', query: { auditID: row.id, type: '2' } });
this.$router.push({ path: '/DetailManagement', query: { auditID: row.id, type: '2' } });
在目标页面通过this.$route.query获取参数：

this.$route.query.type