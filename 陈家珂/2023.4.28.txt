命名路由


除了 path 之外，你还可以为任何路由提供 name。这有以下优点：

没有硬编码的 URL params 的自动编码/解码。 防止你在 url 中出现打字错误。 绕过路径排序（如显示一个）

<!-- UserSettings.vue -->
<div>
  <h1>User Settings</h1>
  <NavBar />
  <router-view />
  <router-view name="helper" />
</div>
{
  path: '/settings',
  // 你也可以在顶级路由就配置命名视图
  component: UserSettings,
  children: [{
    path: 'emails',
    component: UserEmailsSubscriptions
  }, {
    path: 'profile',
    components: {
      default: UserProfile,
      helper: UserProfilePreview
    }
  }]
}