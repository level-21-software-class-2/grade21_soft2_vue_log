# 编程式导航
在 vue 中，实现路由跳转有两种方式：
```
    申明式导航：router-link(一般情况都使用这个)

    编程式导航：this.$router.push()和this.$router.replace()
```
+ 在什么情况下要使用编程式导航？
```
比如在登录页面，点击登录按钮的时候要跳转到系统主页面，这时我们就会用到编程式导航
```
+ this.$router.push(path)
```
是跳转到指定路由，还可以前进和后退，this.$router.go(正数/负数)——正数代表前进几步，负数代表后退几步
```
+ this.$router.replace(path)
```
是替换当前路由跳转到指定路由，不会向 history 添加新纪录，不能前进和后退
```
# 命名路由
+ 除了 path 之外，你还可以为任何路由提供 name。这有以下优点：
+ 没有硬编码的 URL
+ params 的自动编码/解码。
+ 防止你在 url 中出现打字错误。
+ 绕过路径排序（如显示一个）
# 命名视图
+ default
+ 组件名：组件