# Element Plus
## 前端常见的UI组件库：
+ elemet-plus(Vue3.0)
+ ant-design (多在react实现)
+ iview(view design)
## 安装
+ yarn add element-plus
## 全局引入：main.js
```js
import ElementPlus from 'element-plus',
import 'element-plus/dist/index.css'
```
## 用
```js
const app = createApp(App)
app.use(ElementPlus)
app.mount('#app')
```
