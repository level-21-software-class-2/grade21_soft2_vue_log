# element-plus折叠菜单栏

+ template
```html
<template>
  <div class="common-layout">
    <el-container>
      <el-aside :width="isCollapse ? '64px' : '200px'">
        <Navbar :is-collapse="isCollapse" :menus="menuList"></Navbar>
      </el-aside>
      <el-container>
        <el-header>
          <el-icon :class="{ rotateClass: isCollapse }" :size="30" @click="iconClickHandler">
            <fold></fold>
          </el-icon>
        </el-header>
        <el-main>
          <div class="inline-flex" :style="obj">

          </div>
        </el-main>
        <el-footer>Footer</el-footer>
      </el-container>
    </el-container>
  </div>
</template>
```
+ script
```js
const iconClickHandler = function () {
  isCollapse.value = !isCollapse.value
}
```