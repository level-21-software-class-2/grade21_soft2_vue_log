# element-plus menu菜单
## el-sub-menu
```
有分支
```
## el-mune-item
```
无分支
```
## el-icon图标引入方式一
```
1、yarn add @element-plus/icons-vue

2、在main.js中全局引用、注册
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

3、可以开始使用的，vue3中把图标当成一个组件使用：

 <el-icon>
  <component v-if="item.meta && item.meta.icon" :is="item1。meta.icon"></component>
  <setting v-else></setting>
</el-icon>
```
### 简单的菜单栏实现效果
+ 定义一个对象menuList
+ 通过v-for遍历menuList对象，通过判断是否存在分支，渲染到对应的区域
+ ……

```
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="200px">
        <el-menu>
          <template v-for="item in menuList">
            <el-sub-menu v-if="item.children && item.children.length > 0" :index="item.title">
              <template #title>
                <el-icon>
                  <component v-if="item.meta && item.meta.icon" :is="item.meta.icon"></component>
                  <setting v-else></setting>
                </el-icon>
                <span>
                  {{ item.title }}
                </span>
              </template>
              
              <template v-for="item1 in item.children">
                <el-sub-menu v-if="item1.children && item1.children.length > 0" :index="item1.title">
                  <template #title>
                    <el-icon>
                      <component v-if="item1.meta && item1.meta.icon" :is="item1.meta.icon"></component>
                      <setting v-else></setting>
                    </el-icon>
                    <span>
                      {{ item1.title }}
                    </span>
                  </template>
                     
                      <template v-for="item2 in item1.children">
                        <el-sub-menu v-if="item2.children && item2.children.length>0" :index="item2.title">
                          <template #title>
                            <el-icon>
                              <component v-if="item2.meta && item2.meta.icon" :is="item2.meta.icon"></component>
                              <setting v-else></setting>
                            </el-icon>
                            <span>
                              {{item2.title}}
                            </span>
                          </template>
                        </el-sub-menu>
                        <el-menu-item v-else> 
                          <el-icon>
                            <component v-if="item2.meta && item2.meta.icon" :is="item2.meta.icon"></component>
                            <setting v-else></setting>
                          </el-icon>
                            <template #title>
                              <span>
                                {{ item2.title }}
                              </span>
                            </template>
                        </el-menu-item>
                        
                      </template>
                </el-sub-menu>

                <el-menu-item v-else>
                  <el-icon>
                    <component v-if="item1.meta && item1.meta.icon" :is="item1.meta.icon"></component>
                    <setting v-else></setting>
                  </el-icon>
                  <span>
                    {{ item1.title }}
                  </span>

                </el-menu-item>
              </template>


            </el-sub-menu>

            <el-menu-item v-else>
              <span>
                {{ item.title }}
              </span>
            </el-menu-item>
          </template>
        </el-menu>

      </el-aside>
      <el-container>
        <el-header>Header</el-header>
        <el-main>Main</el-main>
        <el-footer>
        </el-footer>
      </el-container>
    </el-container>
  </div>
</template>

<style>
.el-aside {
  background-color: pink;
  height: 100vh;
}

.el-container {
  padding: 0;
  margin: 0;
}

#app {
  margin: 0;
  padding: 0;
  width: 100vw;
}

.el-header {
  background-color: lightblue;
}

.el-main {
  background-color: lightpink;
}

.el-footer {
  background-color: lightgrey;
}
</style>

<script setup>
import { reactive, ref } from 'vue'
const menuList = reactive([
  {
  title: '读者服务',
  meta: { icon: 'service' },
  children:
    [
    {
      title: '图书查询',
      meta: { icon: 'Search' },
      children: [{
        title: '图书信息查询',
        meta: { icon: '' }
      }]
    },

    {
      title: '新书浏览',
      meta: { icon: 'Pointer' },
      children: [{
        title: '新书信息查询',
        meta: { icon: '' }
      }]
    },

    {
      title: '联系图书馆',
      meta: { icon: 'bell' },
      children: [{
        title: '图书馆公告',
        meta: { icon: '' }
      },

      {
        title: '读者意见',
        meta: { icon: '' }
      }]
    },
    {
      title: '读者登记',
      meta: { icon: 'ChatDotRound' },
      children: [{
        title: '借阅信息查询',
        meta: { icon: '' }
      }, {
        title: '用户信息查询',
        meta: { icon: '' }
      }]
    }]
}, {
  title: '借还书终端',
  mata: { icon: 'User' },
  children: [{
    title: '借阅图书',
    meta: { icon: 'Check' }
  }, {
    title: '归还图书',
    meta: { icon: 'CircleCheck' }
  }]
}, {
  title: '管理员终端',
  mata: { icon: 'Lock' },
  children: [{
    title: '用户管理',
    meta: { icon: 'Promotion' }
  }, {
    title: '书籍管理',
    meta: { icon: 'Promotion' },
    children: [{
      title: '图书修改',
      meta: { icon: '' }
    }, {
      title: '图书删除',
      meta: { icon: '' }
    }, {
      title: '图书添加',
      meta: { icon: '' }
    }]
  }, {
    title: '公告管理',
    meta: { icon: 'Promotion' }
  }, {
    title: '建议管理',
    meta: { icon: 'Promotion' }
  }]
}])

</script>
```