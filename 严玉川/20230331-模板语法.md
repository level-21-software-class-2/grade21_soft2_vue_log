## 第2次课-------模板语法

node.js没学好的就让vue来偿还吧🤣

![](./img/study1.jpg)

```vue
<template>
  <div class="hello">
    <h1>{{ message }}</h1>
  </div>
</template>
```

**<template>里面可以理解成就是写HTML的**

**<script>里面是写业务逻辑的**

### 一、文本

数据绑定最常见到的形式就是使用‘Mustache’(双大括号)语法的文本插值

```vue
<script>Message:{{msg}}</span>
```

一般是配合js中的data（）设置数据

```vue
export default {
  name: 'HelloWorld',
  props: {
    msg: String
  }
}
```

### 二、原始的HTML

双大花括号会将数据解析为普通文本，而非HTML代码，，为了输出真正的HTML，需要使用**v-html**指令

```vue
<p>未使用v-HTML：{{ message }}</p>
<p>使用v-HTML：<span v-html="message"></span></p>
```

```vue
export default {
  name: 'Hello Word',
  data() {
    return {
      message: '<a href="#">怎么办怎么办！专业课就要挂科了！！！！</a>'
    }
  }
}
```

![](.\img\study2.jpg)

**innerHTML与innerText的区别就是：一个是渲染文本，另一个是渲染标签的！**

### 三、属性Attribute

**双花括号的形式也不能去处理页面（HTML）中标签的属性**，然而，**可以使用v-bind**

```vue
<div v-bind:id="dynamicId"></div>
```

**解释：v-bind＋：id，，id的值就变成了一个变量dynamicId，这个变量就可以变成修改的**

```vue
<script>
export default {
  name: 'Hello Word',
  data() {
    return {
      message: '<a href="#">怎么办怎么办！专业课就要挂科了！！！！</a>',
      dynamicId:10001
    }
  }
}
</script>
```

![](.\img\study3.jpg)

从图片中可以看到我们动态的把那些值放到属性当中去了

<h4 style="background: red;width:100px;">温馨提示：</h4>

v-bind可以简写成  **：**

```vue
<div :id="dynamicId"></div>
```

效果是一样的就不展示了