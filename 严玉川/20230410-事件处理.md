# 202304010第6次课---事件处理

<h4 style="background-color: aqua;"></h4>

## 一、介绍：

页面上会有很多的页面进行交互，例如用户点击按钮，会触发什么样的事件，这个事件要做什么事情，就会涉及到事件处理。

<h6 style="background-color: red;"></h6>

## 二、事件的监听

**我们可以使用v-on指令(通常缩写成@符号)来监听DOM事件，并在触发时执行一些JavaScript。**用法为：**v-on：click=‘Methodname**或者使用快捷方式**@click=“Methodname”**，然而许多事件处理逻辑会更为复杂，所以直接把JavaScript代码写在v-on指令里面是不可行的，**因此v-on还可以接收一个需要调用的方法名称。**

```vue
<template>
  <div>
    <button @click="count">+1</button>
    {{ counter }}
  </div>
</template>

<script>
export default {
  data() {
    return {
      counter: 0
    }
  },
  methods: {
    count() {
      this.counter++
    }
  }
}
</script>
```

<h6 style="background-color: red;"></h6>

## 三、内联处理器中的方法

除了直接绑定到一个方法，也可以在内联JavaScript语句中调用方法：

```vue
<template>
  <div>
    <button @click="say('hi')">say hi</button>

  </div>
</template>

<script>
export default {
  data() {
    return {
      counter: 0
    }
  },
  methods: {
    say(mes) {
      alert(mes)
    }
  }
}
</script>
```

![](.\img\click.gif)

有时也需要在内联语句处理器中访问原始的DOM事件，可以使用特殊变量**＄event**把他传入方法中：

```vue
<template>
  <div>
    <button @click="say('hi', $event)">say hi</button>

  </div>
</template>

<script>
export default {
  data() {
    return {
      counter: 0
    }
  },
  methods: {
    say(mes, event) {
      alert(event, mes)
      console.log(event, mes);
    }
  }
}
</script>
```

![](.\img\msg.png)
