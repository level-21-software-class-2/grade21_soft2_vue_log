# 202304010第7次课---表单输入绑定

<h4 style="background-color: aqua;"></h4>

你可以用 v-model 指令在表单 `<input>`、`<textarea>` 及 `<select>` 元素上创建双向数据绑定。它会根据控件类型自动选取正确的方法来更新元素。尽管有些神奇，但 `v-model` 本质上不过是语法糖。它负责监听用户的输入事件以更新数据，并对一些极端场景进行一些特殊处理。

```vue
<template>
  <div class="template-m-wrap">
    <div id="inline-handler">
      <!-- 只有在 `key` 是 `Enter` 时调用 `vm.submit()` -->
      <input v-model="msg" placeholder="edit me"/>
    </div>
  </div>
</template>
<script>
export default {
  name: "TemplateM",
  data() {
    return {
      msg: '',
    };
  },
};
</script>
```

<h6 style="background-color: red;"></h6>

## 二、多行文本（text area）

在文本区域插值不起作用，应该使用v-model来代替。

```vue
<template>
  <div>
    <textarea v-model="msg" placeholder="edit me"></textarea>
  </div>
</template>

<script>
export default {
  data() {
    return {
      msg: ''
    }
  },

}
</script>
```

![](.\img\textarea.png)