# 实例
## 数据库表结构
```
CREATE TABLE [dbo].[QD_Router](
    Id INT IDENTITY(1,1) NOT NULL,--唯一id
    SySCName NVARCHAR](50) NULL,--菜单中文名称
    name NVARCHAR(50) NULL ,--菜单英文名称
    SysLayer INT NULL,--菜单等级
    SysUpId INT NULL,--菜单上级id
    )
```

## 修改src\store\modules\user.js 下GetInfo方法
```
GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        QueryUserRole().then(response => {
          console.log(response)

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
```

## 新建dynamicRoutes.js：该文件中定义需要根据用户权限动态挂载显示的路由
```
import Layout from '@/layout'
/**
 * 动态路由，需要根据用户权限动态挂载
 */
const DynamicRoutes = [
  {
    path: '/system',
    component: Layout,
    redirect: '/system/user',
    name: 'System',
    meta: {
      title: '系统管理',
      icon: 'example',
      permission: 'MENU_SYSTEM'
    },
    children: [
      {
        path: 'user',
        name: 'User',
        component: () => import('@/views/system/user/index'),
        meta: {
          title: '用户管理',
          icon: 'table',
          permission: 'MENU_SYSTEM_USER'
        }
      },
      {
        path: 'role',
        name: 'Role',
        component: () => import('@/views/system/role/index'),
        meta: {
          title: '角色管理',
          icon: 'table',
          permission: 'MENU_SYSTEM_ROLE'
        }
      },
      {
        path: 'dict',
        name: 'Dict',
        component: () => import('@/views/system/dict/index'),
        meta: {
          title: '字典管理',
          icon: 'table',
          permission: 'MENU_SYSTEM_DICT'
        }
      }
    ]
  },
]

export default DynamicRoutes
```

## permission.js：该文件用于路由跳转前的权限校验，如：token校验、获取用户信息生成用户动态菜单等
```
import router from './router'
import store from './store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { Message } from 'element-ui'
import { getToken } from '@/utils/auth' // getToken from cookie
import { loginCheck } from "@/api/login";
import asyncRouterMap from './router/dynamicRoutes'


NProgress.configure({ showSpinner: false })// NProgress configuration


const whiteList = ['/login'] // 不重定向白名单


//将后台传输的数据与当前路由对比生成用户所属路由
export function recursionRouter(userRouter = [], allRouter = []) {
  var realRoutes = []
  allRouter.forEach((v) => {

    userRouter.forEach((item) => {
      if (v.name == item.name) {

        v.children = recursionRouter(item.SysLayer, v.children)
        realRoutes.push(v)

      }
    })
  })

  return realRoutes
}
//获取后台传输过来的用户权限
export function arrayToTree(arr, SysUpId) {
  let temp = [];
  let treeArr = arr;
  treeArr.forEach((item, index) => {
    if (item.SysUpId == SysUpId) {
      if (arrayToTree(treeArr, treeArr[index].Id).length > 0) {
        treeArr[index].SysLayer = arrayToTree(treeArr, treeArr[index].Id);
      }
      temp.push(treeArr[index]);
    }
  });
  return temp;
}
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done() /
    } else {
      if (store.getters.roles.length === 0) {
        store.dispatch('GetInfo').then(res => {
         let Hroel = arrayToTree(res,0)
         let newRole =  recursionRouter(Hroel,asyncRouterMap)
         router.addRoutes(newRole)
         router.options.routes = newRole
          //在每次刷新时校验token是否过期
          loginCheck(getToken()).then(result => {
            if (result.code != 200) {
              store.dispatch('FedLogOut').then(() => {
                Message.error(err || '登录失效请重新登录')
                next({ path: '/' })
              })
            }
          })
          next()
        }).catch((err) => {
          store.dispatch('FedLogOut').then(() => {
            Message.error(err || '登录失效请重新登录')
            next({ path: '/' })
          })
        })
      } else {
        next()
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})
router.afterEach(() => {
  NProgress.done() // 结束Progress
})
```