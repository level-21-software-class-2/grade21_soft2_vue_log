Vue Router 是 Vue.js 的官方路由。它与 Vue.js 核心深度集成，让用 Vue.js 构建单页应用变得轻而易举。功能包括：

* 嵌套路由映射
* 动态路由选择
* 模块化、基于组件的路由配置
* 路由参数、查询、通配符
* 展示由 Vue.js 的过渡系统提供的过渡效果
* 细致的导航控制
* 自动激活 CSS 类的链接
* HTML5 history 模式或 hash 模式
* 可定制的滚动行为
* URL 的正确编码

# 安装
* npm
```
npm install vue-router
```
* yarn
```
yarn add vue-router
```

# 入门
## main.js
```js
import { createApp } from 'vue'
import App from './App.vue'
import {createRouter,createWebHashHistory} from 'vue-router'

// 1. 定义路由组件.
// 也可以从其他文件导入
import Home from './components/Home.vue'
import About from './components/About.vue'

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

// 5. 创建并挂载根实例
const app = createApp(App)
//确保 _use_ 路由实例使
//整个应用支持路由。
app.use(router)

app.mount('#app')

// 现在，应用已经启动了！



```

## App.vue
```
<script setup>
</script>

<template>

  <div>
    <router-view></router-view>
  </div>
</template>



```

## Center.vue
```
<template>
    Center
    <router-link to="/home">Home</router-link>
    <br>
    <router-link to="/about">About</router-link>
</template>
```

## About.vue
```
<template>
    About
    <router-link to="/">Center</router-link>
</template>
```

## Home.vue
```
<template>
    Home
    <router-link to="/">Center</router-link>
</template>
```

# 去除#号

```js
const router = createRouter({
    history: createWebHistory(),//使用createWebHistory便可以去除#号
    routes, 
})
```
# Vue路由中的hash和history模式的区别及原理
## hash模式
### 优点
* 兼容性强，兼容性达到了IE8
* 除发送ajax和资源请求外不会发送其他多余请求
* 改变#后的路径、不会自动刷新页面
* 无需服务端进行配合
### 缺点
* 访问路径上包含#,不美观
* 对于需要锚点功能的需求会与当前路由机制发生冲突
* 重定向操作时，后段无法获取url完整路径。

## history模式
### 优点
* 符合url地址规范, 不需要#, 使用起来比较美观
* 可以使用history.state获取完整的路由信息
* 后端可以获取到完整的路由信息
### 缺点
* 兼容性只到IE10
* 改变url路径后、会重新请求资源。
* 若访问的路由地址不存在时、会报404,需服务端配合支持重定向返回统一的404页面。
## 原理
* hash模式灵活运用了html的瞄点功能、改变#后的路径本质上是更换了当前页面的瞄点，所以不会刷新页面。
* history是使用了 H5 提供的pushState() 和 replaceState()，允许开发者直接更改前端路由，即更新浏览器 URL 地址而不重新发起请求(将url替换并且不刷新页面)。