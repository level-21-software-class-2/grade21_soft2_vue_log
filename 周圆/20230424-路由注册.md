# 路由

```js
// main.js
import { createApp } from 'vue'
import App from './App.vue'
import {createRouter,createWebHashHistory} from 'vue-router'

// 1. 定义路由组件.
// 也可以从其他文件导入
import index from './components/index.vue'
import home from './components/home.vue'
import about from './components/about.vue'


// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { path: '/', component: index },
  { path: '/home', component: home },
  { path: '/about', component: about },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})


const app =createApp(App)
//确保 _use_ 路由实例使
//整个应用支持路由。
app.use(router)

app.mount('#app')

```



```js
//App.vue

<template>
<div>
<!-- 表示路由的出口 -->
  <router-view></router-view>
</div>

</template>


//index.vue
<template>
    <div>
        首页
        <br>
        <!-- 路由的入口 -->
        <RouterLink to='/home'>去home</RouterLink>
        <br>
        <RouterLink to="/about">去abute</RouterLink>
    </div>
</template>
```


#### 模式的不同
createWebHashHistory 是hash模式，路由上会有#
createWebHistory     是history模式，路由上不带#