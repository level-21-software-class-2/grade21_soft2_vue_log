## 计算属性
```js
    
export default{
  data(){
    return {
      stu:{
        name :'xz',
        cj :{
          english :60,
          math : 80,
          japn : 20
        }
      }
    }
  },

  computed :{
    //计算机属性的getter，只做计算
    //计算属性值会被缓存，响应式更新时才会重新计算
    xyz(){
      let obj = this.stu.cj;
      let cc = 0
      for(let key in this.stu.cj){
        if(obj[key]>=60){
          cc++;
        }  
      }
      return cc
    }
    
  }
}
```
调用xyz得到计算结果

## 类与样式的绑定

```js
export default{
    data() {
  return {
    isActive: true,
    hasError: false
  }
    }
}
<p :class ="{as :isActive}">阿斯顿撒多</p>

```
通过isActive的值来判断class = as 存不存在