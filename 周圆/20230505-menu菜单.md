```js
<script setup>
import { reactive } from 'vue'

const list = reactive([
  {
    title: '主页',
    meta: { icon: 'House' },
    children: [
      {
        title: '学院',
        meta: { icon: 'School' }
      },
      {
        title: '教师',
        meta: { icon: 'Service' }
      },
      {
        title: '学生',
        meta: { icon: 'User' },
        children: [
          {
            title: '成绩',
            meta: { icon: 'Menu' },
          },
          {
            title: '获奖情况',
            meta: { icon: 'Histogram' },
          }
        ]
      }
    ]

  },
  {
    title: '公告',
    meta: { icon: 'List' },
  }
])
</script>
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="200px">
        <el-menu>
          <template v-for="menu in list">
            <el-sub-menu v-if="menu.children && menu.children.length > 0">
              <template #title>
                <el-icon>
                  <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                </el-icon>
                {{ menu.title }}
              </template>
              <template v-for="subMenu in menu.children">
                <el-sub-menu v-if="subMenu.children && subMenu.children.length > 0">
                  <template #title>
                    <el-icon>
                    <component v-if="subMenu.meta && subMenu.meta.icon" :is="subMenu.meta.icon"></component>
                  </el-icon>
                    {{ subMenu.title }}
                  </template>
                  <el-menu-item v-for="subList in subMenu.children">
                    <el-icon>
                      <component v-if="subList.meta && subList.meta.icon" :is="subList.meta.icon"></component>
                    </el-icon>
                    {{ subList.title }}
                  </el-menu-item>
                </el-sub-menu>
                <el-menu-item v-else>
                  <el-icon>
                  <component v-if="subMenu.meta && subMenu.meta.icon" :is="subMenu.meta.icon"></component>
                </el-icon>
                  {{ subMenu.title }}
                </el-menu-item>
              </template>
            </el-sub-menu>
            <el-menu-item v-else>
              <el-icon>
                  <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                </el-icon>
              {{ menu.title }}
            </el-menu-item>
          </template>
        </el-menu>
      </el-aside>
      <el-container>
        <el-header>
          Header
        </el-header>
        <el-main>
          Main
        </el-main>
        <el-footer>
          Footer
        </el-footer>
      </el-container>
    </el-container>
  </div>
</template>


<style scoped>
.el-aside {
  background-color: grey;
  height: 100vh;
}

.el-header {
  background-color: lightslategrey;
}

.el-main {
  background-color: darkgrey;
}

.el-footer {
  background-color: dimgrey;
}
</style>
```