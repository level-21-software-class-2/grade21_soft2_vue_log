# Props声明
* 父组件向子组件传递参数
```js
1.定义props
export default {
  props: {
    title: String,
    likes: Number
  }
}
```
# Props名字格式
```js
//定义了一个很长的名字(小驼峰)
export default {
  props: {
    greetingMessage: String
  }
}
//使用的时候就是必须和定义的一模一样
<span>{{ greetingMessage }}</span>
//推荐这种命名格式，也可以不加-，直接和原来的一样
<MyComponent greeting-message="hello" />

```
# 单向数据流
只能从父组件的更新而变化，在子组件上去更改prop，就会错误

# 补充小细节
```js
//必填
1.所有 prop 默认都是可选的，除非声明了 required: true。
//未传入值默认undefined
2.除 Boolean 外的未传递的可选 prop 将会有一个默认值 undefined。

3.Boolean 类型的未传递 prop 将被转换为 false。这可以通过为它设置 default 来更改——例如：设置为 default: undefined 将与非布尔类型的 prop 的行为保持一致。

4.如果声明了 default 值，那么在 prop 的值被解析为 undefined 时，无论 prop 是未被传递还是显式指明的 undefined，都会改为 default 值。
```