## 左侧菜单栏的包装

```
app.vue
```vue
<template>
  <div>
    <el-container>
      <el-aside width="200px">
        <nav-menu :menus="menus"></nav-menu>
      </el-aside>
      <el-container>
        <el-header>
          <tb></tb>
        </el-header>
        <el-main>Main</el-main>
        <el-footer>
          <db></db>
        </el-footer>
      </el-container>
    </el-container>
  </div>
</template>
<script>
import db from './components/DB'
import NavMenu from './components/Nav/NavMenu.vue';
import tb from './components/TB'
export default {
  components:{
    db,
    tb,
    NavMenu
  },
  data: function () {
    return {
      menus: [
        {
          title: "我裂开了",
          path: "/ahc",
        },
        {
          title: "文件管理",
          path: "/bhc",
          children: [
            {
              title: "图片",
              path: "bbhc",
            },
            {
              title: "视频",
              path: "bbbhc",
            },
            {
              title: "img",
              path: "bbbbbhc",
            },
          ],
        },
        {
          title: "音乐管理",
          path: "/chc",
          children: [
            {
              title: "周杰伦",
              path: "zjl",
            },
            {
              title: "林俊杰",
              path: "ljj",
            },
          ],
        },
      ],
    };
  },
  
};
</script>

<style>
.el-col-12 {
  width: 100% !important;
}
.el-submenu .el-menu-item {
  height: 50px;
  line-height: 50px;
  padding: 0 45px;
  min-width: 100% !important;
}
.el-header,
.el-footer {
  background-color: #b3c0d1;
  color: #333;
  text-align: center;
  line-height: 60px;
}

.el-aside {
  background-color: #d3dce6;
  color: #333;
  text-align: center;
  line-height: 17px;
}
  .el-menu-vertical-demo:not(.el-menu--collapse) {
    width: 200px;
    min-height: 100vh;
  }
.el-main {
  background-color: #e9eef3;
  color: #333;
  text-align: center;
  line-height: 160px;
}

body > .el-container {
  margin-bottom: 40px;
}

.el-container:nth-child(5) .el-aside,
.el-container:nth-child(6) .el-aside {
  line-height: 260px;
}

.el-container:nth-child(7) .el-aside {
  line-height: 320px;
}
.el-container {
  min-height: 100vh;
}
</style>
```

 navmenu.vue

```
<template>
    <div>
        <el-menu
            default-active="2"
            class="el-menu-vertical-demo"
            @open="handleOpen"
            @close="handleClose"
            background-color="#545c64"
            text-color="#fff"
            active-text-color="#ffd04b"
            unique-opened
          >
          <nav-menu-item v-for="menu in menus" :menu="menu" :key="menu.path"></nav-menu-item>
        </el-menu>
    </div>
</template>
<script>
import NavMenuItem from './NavMenuitem.vue'
export default {
    components:{
        NavMenuItem
    },
    props:['menus'],
    methods: {
    handleOpen(key, keyPath) {
      console.log(key, keyPath);
    },
    handleClose(key, keyPath) {
      console.log(key, keyPath);
    },
  },
}
</script>
```

 navmenuitem.vue

```
<template>
  <div>
    <template v-if="menu.children && menu.children.length > 0">
      <el-submenu :index="menu.path" :key="menu.path">
        <template slot="title">
          <i class="el-icon-location"></i>
          <span>{{ menu.title }}</span>
        </template>
        <template>
          <nav-menu-item
            v-for="item in menu.children"
            :menu="item"
            :key="item.path"
          ></nav-menu-item>
        </template>
      </el-submenu>
    </template>
    <template v-else>
      <el-menu-item :index="menu.path" :key="menu.path" >
        <i class="el-icon-menu"></i>
        <span slot="title">{{ menu.title }}</span>
      </el-menu-item>
    </template>
  </div>
</template>
   

<script>
export default {
  name: "NavMenuItem",

  props: ["menu"],
};
</script>
```