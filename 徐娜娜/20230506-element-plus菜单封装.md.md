## 代码

```
<template>
    <div>
        <!-- 遍历传入的菜单数组 -->
        <template v-for="menu in itemMenus">
            <!-- 当前菜单对象如果有下级菜单，即有children属性，并且children的长度大于0，则需要使用组件中的子菜单组件el-sub-menu -->
            <el-sub-menu index="1" v-if="menu.children && menu.children.length > 0">
                <template #title>
                    <el-icon>
                        <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                        <setting v-else></setting>
                    </el-icon>
                    {{ menu.title }}
                </template>
                <!-- 重复自己 -->
                <HomeList :item-menus="menu.children"></HomeList>
            </el-sub-menu>
            <el-menu-item index="2" v-else>
                <el-icon>
                    <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                    <setting v-else></setting>
                </el-icon>
                {{ menu.title}}
            </el-menu-item>
        </template>
    </div>
</template>
<script setup>
const props = defineProps(['itemMenus'])

</script>xxxxxxxxxx // 如果您正在使用CDN引入，请删除下面一行。import * as ElementPlusIconsVue from '@element-plus/icons-vue'const app = createApp(App)for (const [key, component] of Object.entries(ElementPlusIconsVue)) {  app.component(key, component)}
```