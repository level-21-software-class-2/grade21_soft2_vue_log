<script setup>
import A1 from './components/A1.vue'
import A2 from './components/A2.vue'
import { ref } from 'vue'
const currentTab =ref('A1');
const Tabs ={
  A1,
  A2
}
</script>

<template>
  <div class="demo">
    <button v-for="(_,tab) in Tabs" @click="currentTab=tab" :key="tab"
       :class="['tab-button', { active: currentTab === tab }]">
      
      
      {{ tab }}</button>
    <component :is="Tabs[currentTab]" class="tab"></component>
  </div>
  



</template>

<style scoped>
header {
  line-height: 1.5;
}

.logo {
  display: block;
  margin: 0 auto 2rem;
}

@media (min-width: 1024px) {
  header {
    display: flex;
    place-items: center;
    padding-right: calc(var(--section-gap) / 2);
  }

  .logo {
    margin: 0 2rem 0 0;
  }

  header .wrapper {
    display: flex;
    place-items: flex-start;
    flex-wrap: wrap;
  }
}
</style>
