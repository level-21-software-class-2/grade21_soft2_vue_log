
# 生命周期
## 生命周期钩子
每个 Vue 组件实例在创建时都需要经历一系列的初始化步骤，比如设置好数据侦听，编译模板，挂载实例到 DOM，以及在数据改变时更新 DOM。在此过程中，它也会运行被称为生命周期钩子的函数，让开发者有机会在特定阶段运行自己的代码
## 注册周期钩子
mounted 钩子可以用来在组件完成初始渲染并创建 DOM 节点后运行代码：
```JS
export default {
  mounted() {
    console.log(`the component is now mounted.`)
  }
}
```
还有其他一些钩子，会在实例生命周期的不同阶段被调用，最常用的是 mounted、updated 和 unmounted

所有生命周期钩子函数的 this 上下文都会自动指向当前调用它的组件实例。注意：避免用箭头函数来定义生命周期钩子，因为如果这样的话你将无法在函数中通过 this 获取组件实例
## 生命周期图示
[图裂了!](./imgs/%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%9B%BE%E7%A4%BA2.png)
# 侦听器
## 基本事例
```JS
<script>
  export default {
    data() {
      return {
        question:'',
        answer:''
      }
    },
    watch: {
      question(newQuestion,oldQuestion){
        if(newQuestion.includes('你是谁')){
          this.getAnswer()
        }
      }
    },
    methods: {
      async getAnswer() {
        this.answer = '我是A'
      }
    }
  }
</script>
<template>
<p>
  问一个问题:
  <input v-model="question" />
</p>
<p>{{ answer }}</p>
</template>
```
## 深层侦听器
watch 默认是浅层的：被侦听的属性，仅在被赋新值时，才会触发回调函数——而嵌套属性的变化不会触发。如果想侦听所有嵌套的变更，需要深层侦听器：
```JS
export default {
  watch: {
    someObject: {
      handler(newValue, oldValue) {
        // 注意：在嵌套的变更中，
        // 只要没有替换对象本身，
        // 那么这里的 `newValue` 和 `oldValue` 相同
      },
      deep: true
    }
  }
}
```
深度侦听需要遍历被侦听对象中的所有嵌套的属性，当用于大型数据结构时，开销很大。因此请只在必要时才使用它，并且要留意性能
## 即时回调的侦听器
watch 默认是懒执行的：仅当数据源变化时，才会执行回调。但在某些场景中，我们希望在创建侦听器时，立即执行一遍回调。举例来说，我们想请求一些初始数据，然后在相关状态更改时重新请求数据

我们可以用一个对象来声明侦听器，这个对象有 handler 方法和 immediate: true 选项，这样便能强制回调函数立即执行：

```JS
export default {
  // ...
  watch: {
    question: {
      handler(newQuestion) {
        // 在组件实例创建时会立即调用
      },
      // 强制立即执行回调
      immediate: true
    }
  }
  // ...
}
```
回调函数的初次执行就发生在 created 钩子之前。Vue 此时已经处理了 data、computed 和 methods 选项，所以这些属性在第一次调用时就是可用的
## 回调的触发时机
当你更改了响应式状态，它可能会同时触发 Vue 组件更新和侦听器回调

默认情况下，用户创建的侦听器回调，都会在 Vue 组件更新`之前`被调用。这意味着你在侦听器回调中访问的 DOM 将是被 Vue 更新之前的状态

如果想在侦听器回调中能访问被 Vue 更新`之后`的 DOM，你需要指明 flush: 'post' 选项：

```JS
export default {
  // ...
  watch: {
    key: {
      handler() {},
      flush: 'post'
    }
  }
}
```
## this.$watch()
使用组件实例的 $watch() 方法来命令式地创建一个侦听器：
```JS
export default {
  created() {
    this.$watch('question', (newQuestion) => {
      // ...
    })
  }
}
```
如果要在特定条件下设置一个侦听器，或者只侦听响应用户交互的内容，这方法很有用。它还允许你提前停止该侦听器
## 停止侦听器
用 watch 选项或者 $watch() 实例方法声明的侦听器，会在宿主组件卸载时自动停止

在少数情况下，你的确需要在组件卸载之前就停止一个侦听器，这时可以调用 $watch() API 返回的函数：
```JS
const unwatch = this.$watch('foo', callback)

// ...当该侦听器不再需要时
unwatch()
```