# 安装

## 使用包管理器

我们建议您使用包管理器（如 NPM、Yarn 或 pnpm）安装 Element Plus，然后您就可以使用打包工具，例如 Vite 或 webpack。

### 选择一个你喜欢的包管理器

- NPM

```
$ npm install element-plus --save
```

- Yarn

```
$ yarn add element-plus
```

- pnpm

```
$ pnpm install element-plus
```

# 引入

```js
import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("./components/HelloWorld.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

createApp(App).use(ElementPlus).use(router).mount("#app");
```

# 任务

实现布局高度自适应

```
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="200px">Aside</el-aside>
      <el-container>
        <el-header>Header</el-header>
        <el-main>Main</el-main>
        <el-footer>Footer</el-footer>
      </el-container>
    </el-container>
  </div>
</template>


<style scoped>

.el-aside{
  background-color: #f3d19e;
  height: 100vh;
}

.el-header{
  background-color: #fcd3d3;
}

.el-main{
  background-color: #dedfe0;
}

.el-footer{
  background-color: #d1edc4;
}
</style>


```
