路由基础
```
    主要分为五步
    1、定义路由组件
    2、定义路由
    3、创建路实例并传递routes配置
    4、history模型的实现
    5、创建并挂载根实例
```
1、定义路由组件
    新建组件
    或者
    定义路由
    const Home = { template: '<div>Home</div>' }
    const About = { template: '<div>About</div>' }
2、定义路由
    ！！！每个路由对应一个组件
const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
]
3、创建路由实例并传递 routes 配置
    const router =createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history:createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})
5、创建并挂载根实例
const app = createApp(App) //确保 use 路由实例使 //整个应用支持路由。 app.use(router)

app.mount('#app')